const WebSocketServer = new require('ws');

const users = {};
const messageHistory = [];
const allChat = ['chat1'];

function User(){
    this.connect = null;
    this.id = null;
    this.name = null;
    this.chat = 'chat1';
    this.status = 'offline';
}

const webSocketServer = new WebSocketServer.Server({port: 3000});

webSocketServer.on('connection', function(ws) {
    let id = Math.floor(Math.random() * 1000000);

    ws.on('message', function(message) {
        let newMessage = JSON.parse(message);

        switch (newMessage.msgType) {
            case 'login':
                for (let key in users) {

                    if (users[key].name === newMessage.userName && users[key].status === 'online') {
                
                        ws.send(JSON.stringify({msgType: 'userOnConnect',}));
                        return;
                    } 
                }

                for (let key in users) {

                    if (users[key].name === newMessage.userName && users[key].status === 'offline') {

                        users[key].connect = ws;
                        users[key].chat = 'chat1';
                        users[key].status = 'online';
                        id = users[key].id;
                        users[key].connect.send(JSON.stringify({
                            msgType: 'userOffConnect',
                            chat: users[key].chat,
                            userName: users[key].name,
                            messageHistory,
                            users,
                        }));
                        const forUsers = JSON.stringify({
                            chat: users[key].chat, 
                            userName: users[key].name, 
                            msgType: 'updateUsersConnect',
                        });
                        for (let key in users) {
                            users[key].connect.send(forUsers);
                        }
                        return;
                    }
                }

                users[id] = new User;
                users[id].name = newMessage.userName;
                users[id].connect = ws;
                users[id].chat = 'chat1';
                users[id].id = id;
                users[id].status = 'online';
                ws.send(JSON.stringify({msgType: 'newUserConnect',}));
            break;

            case 'message':

                    const msgToChat = JSON.stringify({
                        messageText: newMessage.messageText,
                        userName: users[id].name,
                        userID: users[id].id,
                        chat: newMessage.chat,
                        msgType: 'chatMessage',
                    });

                    messageHistory.push(msgToChat);
                    
                    for (let key in users) {   
                        users[key].connect.send(msgToChat);
                    }
            
                    let brigtUser = JSON.stringify({
                        userName: users[id].name,
                        chat: newMessage.chat,
                        msgType: 'userHighlight',
                    });
            
                    users[id].connect.send(brigtUser);
                break;

            case 'createChat':
                    allChat.push(newMessage.chatName);
                    for (let key in users) {   
                        users[key].connect.send(JSON.stringify({
                            msgType: 'createChat',
                            chatName: newMessage.chatName,
                        }));
                    }
                break;

            default:
                break;
        } 
    });

    ws.on('close', function() {
        users[id].status = 'offline';

        for (let chat of allChat) {
            let endConnect = JSON.stringify({userName: users[id].name, chat: chat, msgType: 'userOffline',});
            for (let key in users) {
                users[key].connect.send(endConnect);
            }
        }
    });
});
