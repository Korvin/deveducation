const socket = new WebSocket('ws://localhost:3000');
const enterBtn = document.getElementById('enter-btn');
const sendBtn = document.getElementById('send-btn');
const userMessage = document.getElementById('message');
const inputChat = document.getElementById('name-chat');
let loginName;

let message = {
    userName: '',
    messageText: '',
    userId: null,
    userConnect: '',
    chat: 'chat1',
    userStatus: 'offline',
    msgType: 'login',
};

document.addEventListener('click', changeChat);

document.forms.login.onsubmit = function () {
    message.userName = document.getElementById('input-name').value;
    message.msgType = 'login';
    socket.send(JSON.stringify(message));
    return false;
};

document.forms.publish.onsubmit = function () {
    message.msgType = 'message';

    if (userMessage.value) {
		message.messageText = this.message.value;

		socket.send(JSON.stringify(message));
		userMessage.value = '';
		return false;
    }
    return false;
};

document.forms.chat.onsubmit = function () {
    message.chatName = document.getElementById('name-chat').value;
    message.msgType = 'createChat';
    socket.send(JSON.stringify(message));
    inputChat.value = '';
    return false;
};

socket.onmessage = function (event) {
    const serverMessage = event.data;
    const message = JSON.parse(serverMessage);

    switch (message.msgType) {
        case 'userOffline':
            showUserConnect(message);
            break;
        case 'chatMessage':
            showMessage(message);
            setUserToChat(message);
            break;
        case 'userHighlight':
            userHighlight(message);
            break;
        case 'newUserConnect':
            enterToChat();
            break;
        case 'userOnConnect':
            showMessageUserExists();
            break;
        case 'userOffConnect':
            enterToChat();
            showMessageOldUser();
            updateHistory(message.messageHistory);
            updateUsersList(message.users);
            userHighlight(message);
            break;
        case 'updateUsersConnect':
            setUserConnectGreen(message);
            break;
        case 'createChat':
            
            break;
        default:
            break;
    }
};

function updateUsersList(users) {
    for(const [userId, user] of Object.entries(users)) {
        setUserToChat({
            chat: user.chat,
            userName: user.name,
        });
    }
}

function updateHistory(messageHistory) {
    messageHistory.forEach(item => showMessage(JSON.parse(item)));
}

function showMessageUserExists() {
    alert('This name is already exists');
}

function showMessageOldUser() {
    alert('Hello old User');
}

function userHighlight(serverMessage) {
    const clientLists = [...document.querySelectorAll('.users-block')];
    const currentClientList = clientLists.filter(elem => elem.dataset.id === serverMessage.chat);

    if (currentClientList[0].firstElementChild === null) {
        return false;
    }

    const currentClient = [...currentClientList[0].children].filter(elem => elem.dataset.name === serverMessage.userName)
    currentClient[0].style.background = 'orange';
}

function showMessage(serverMessage) {
    const chatFields = [...document.querySelectorAll('.conversation__fild')];
    const currentChatField = chatFields.filter(elem => elem.dataset.id === serverMessage.chat);

    if (serverMessage === null || serverMessage.messageText === '' || serverMessage.messageText === undefined) {
        return false;
    }

    const messageElement = document.createElement('div');

    messageElement.append(document.createTextNode(`${serverMessage.userName}: ${serverMessage.messageText}`));
    currentChatField[0].append(messageElement);
}

function setUserToChat(serverMessage) {
    const clientLists = [...document.querySelectorAll('.users-block')];
    const currentClientList = clientLists.filter(elem => elem.dataset.id === serverMessage.chat);
    const activeUsers = [...currentClientList[0].children];

    if (activeUsers[0]) {
        const activeUsersName = activeUsers.map(elem => elem.lastElementChild);
        
        for (let elem of activeUsersName) {
            if (elem.textContent === serverMessage.userName || serverMessage.userName === undefined) {
                return false;
            }
        }
    }

    const newBlock = document.createElement('div');
    const userStatus = document.createElement('span');
    const newUser = document.createElement('span');
    
    newBlock.classList.add('user__user-style');
    newBlock.dataset.name = serverMessage.userName;
    userStatus.classList.add('user-status');
    newUser.classList.add('user-name');

    newBlock.append(userStatus);
    newBlock.append(newUser);
    newUser.textContent = serverMessage.userName;
    currentClientList[0].append(newBlock);
}

function enterToChat() {
    const loginBlock = document.getElementById('login-block');
    const wrapperChat = document.getElementById('wrapper-chat');
    const inputName = document.getElementById('input-name');

    if (inputName.value.length <= 3) {
        alert('Enter "Name" more then 3 simbols');
        return false;
    }

    loginName = inputName.value;
    loginBlock.style.display = 'none';
    wrapperChat.style.display = 'flex';

    showSelectChat();
}

function changeChat(event) {
    const target = event.target;

    if (target.matches('.chat-list__li')) {

        const chats = [...document.querySelectorAll('.chat-list__li')];

        for (let chat of chats) {
            chat.classList.remove('chat-list__li--active-chat');
        }

        target.classList.add('chat-list__li--active-chat');
        message.chat = target.dataset.value;

        showSelectChat();
    }
}

function showSelectChat() {
    const chatFields = [...document.querySelectorAll('.conversation__fild')];
    const currentChatField = chatFields.filter(elem => elem.dataset.id === message.chat);
    chatFields.forEach(elem => elem.style.display = 'none');
    currentChatField[0].style.display = 'block';

    const clientLists = [...document.querySelectorAll('.users-block')];
    const currentClientList = clientLists.filter(elem => elem.dataset.id === message.chat);
    clientLists.forEach(elem => elem.style.display = 'none');
    currentClientList[0].style.display = 'block';
}

function showUserConnect(serverMessage) {
    const clientLists = [...document.querySelectorAll('.users-block')];
    const currentClientList = clientLists.filter(elem => elem.dataset.id === serverMessage.chat);
    if (currentClientList[0]) {
        const users = [...currentClientList[0].children];

        for (let elem of users) {
            if (elem.textContent === serverMessage.userName) {
                elem.firstElementChild.style.backgroundColor = 'red';
            }
        }
    }
}

function setUserConnectGreen(serverMessage) {
    const clientLists = [...document.querySelectorAll('.users-block')];
    const currentClientList = clientLists.filter(elem => elem.dataset.id === serverMessage.chat);
    if (currentClientList[0]) {
        const users = [...currentClientList[0].children];

        for (let elem of users) {
            if (elem.textContent === serverMessage.userName) {
                elem.firstElementChild.style.backgroundColor = 'green';
            }
        }
    }
}
