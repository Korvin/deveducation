const btnBlock = document.getElementById('btn-block');
const inputBlock = document.getElementById('input-block');
const table = document.getElementById('table-block');
const titleRow = document.getElementById('table-info');

const inputId = document.getElementById('input-id');
const inputFirstName = document.getElementById('first-name');
const inputLastName = document.getElementById('last-name');
const inputAge = document.getElementById('input-age');

const dataForm = {
    id: '',
    firstName: '',
    lastName: '',
    age: '',
};

btnBlock.addEventListener('click', selectBtn);
table.addEventListener('click', selectRow);

function selectBtn(event) {
    buttonType = event.target.id;
    switch (buttonType) {
        case 'add-start':
            addRowToBegin();
            break;
        case 'add-middle':
            addRowToMiddle()
            break;
        case 'add-end':
            addRowToEnd();
            break;
        case 'save':
            saveToLocal();
            break;
        case 'read':
            readFromLocal();
            break;
        case 'clear':
            clearTable();
            break;
        case 'update':
            changeSelectData();
            break;
        case 'delete':
            deleteSelectData();
            break;
        default: 
			break;
    }
}


function saveToLocal() {
    localStorage.setItem('table', table.innerHTML);
}

function readFromLocal() {
    table.innerHTML = localStorage.getItem('table');
}

function selectRow(event) {
    const parentElement = event.target.parentElement;

    if (parentElement.tagName !== 'TR' || parentElement.hasAttribute('id')) {
        return void 0;
    }

    inputId.value = parentElement.firstElementChild.textContent;
    inputFirstName.value = parentElement.children[1].textContent;
    inputLastName.value = parentElement.children[2].textContent;
    inputAge.value = parentElement.children[3].textContent;
}

function updateData() {
    dataForm.id = inputId.value;
    dataForm.firstName = inputFirstName.value;
    dataForm.lastName = inputLastName.value;
    dataForm.age = inputAge.value;
}

function createNewRow() {
    const newRow = document.createElement('tr');

    updateData();

    for (let key in dataForm) {
        const td = document.createElement('td');

        if (key === 'id') {
            newRow.dataset.id = dataForm[key];
        }

        td.textContent = dataForm[key];
        newRow.append(td);
    }

    return newRow;
}

function addRowToBegin() {

    if (!checkInputsForNull()) {
        return false;
    }

    const titleRow = document.getElementById('table-info');
    const newRow = createNewRow();
    const rows = [...table.rows];
    const checkID = rows.every(elem => elem.dataset.id !== dataForm.id);

    if (checkID) {
        titleRow.after(newRow);
        clearInputs();
    } else {
        alert('Строка с таким ID уже существует');
    }
}

function addRowToMiddle() {

    if (!checkInputsForNull()) {
        return false;
    }

    const table = document.getElementById('table-block');
    const newRow = createNewRow();
    const rows = [...table.rows];
    const checkID = rows.every(elem => elem.dataset.id !== dataForm.id);

    if (checkID) {
        const middleRow = (rows.length % 2 !== 0) ? (rows.length - 1) / 2 : Math.floor((rows.length - 1) / 2);
        const prevRow = rows[middleRow];
        prevRow.after(newRow);
        clearInputs();
    } else {
        alert('Строка с таким ID уже существует');
    }
}

function addRowToEnd() {

    if (!checkInputsForNull()) {
        return false;
    }

    const table = document.getElementById('table-block');
    const newRow = createNewRow();
    const rows = [...table.rows];
    const checkID = rows.every(elem => elem.dataset.id !== dataForm.id);

    if (checkID) {
        table.firstElementChild.append(newRow);
        clearInputs();
    } else {
        alert('Строка с таким ID уже существует');
    }
}

function changeSelectData() {

    if (!checkInputsForNull()) {
        return false;
    }

    const childrens = table.querySelectorAll('tr:not(:nth-child(1))');

    updateData();

    for (let key of childrens) {

        if (dataForm.id ===  key.children[0].textContent) {

            key.children[1].textContent = dataForm.firstName;
            key.children[2].textContent = dataForm.lastName;
            key.children[3].textContent = dataForm.age;

            clearInputs();
            updateData();

            return true;
        }
    }
    alert('Такого ID нет');
}

function deleteSelectData() {

    if (!checkInputsForNull()) {
        return false;
    }

    const childrens = table.querySelectorAll('tr:not(:nth-child(1))');

    updateData();

    for (let key of childrens) {

        if (dataForm.id ===  key.children[0].textContent) {

            key.remove();
            clearInputs();
            updateData();

            return true;
        }
    }
    alert('Такого ID нет');
}

// function saveToLocal() {
//     const saveBlocks = table.querySelectorAll('tr:not(:nth-child(1))');
//     localStorage.setItem('table', saveBlocks.innerHTML);
// }

function clearInputs() {
    let inputs = document.querySelectorAll('input');

    for (let input of inputs) {
        input.value = '';
    }
}

function clearTable() {
    const titleRow = document.getElementById('table-info');

    while(titleRow.nextElementSibling) {
        titleRow.nextElementSibling.remove();
    }
}

function checkInputsForNull() {
    if (inputId.value.trim() && inputFirstName.value.trim() && inputLastName.value.trim() && inputAge.value.trim() !== '') {
        return true;
    }
    return false;
}