const btnBlock = document.getElementById('btn-block');
const table = document.getElementById('table-block');
const tableBody = document.querySelector('tbody');
const select = document.getElementById('select');

const inputId = document.getElementById('input-id');
const inputFirstName = document.getElementById('first-name');
const inputLastName = document.getElementById('last-name');
const inputAge = document.getElementById('input-age');

const dataForm = {
    firstName: '',
    lastName: '',
    age: '',
};

let typeStorage;

select.addEventListener('change', selectDB);
btnBlock.addEventListener('click', selectAction);
tableBody.addEventListener('click', selectRow);

async function selectDB() {
    typeStorage = select.value;
    await getTables();
}

function selectAction(event) {
    buttonType = event.target.id;
    switch (buttonType) {
        case 'create':        
            addServe();
            clearInputs();
            break;
        case 'update':
            updateServe();
            clearInputs();
            break;
        case 'delete':
            deleteServe();
            clearInputs();
            break;
        default: 
			break;
    }
}

async function getTables() {
    let response = await fetch(`http://localhost:3000/${typeStorage}`, {
        method: 'GET',
        body: null
    });

    if (response.ok) {
        createNewTable(await response.json());
    } else {
        alert(`Ошибка HTTP: ${response.status}`);
    }
    clearInputs();
}

async function addServe() {

    if (!checkInputsForNull()) {
        return false;
    }

    updateData();

    let response = await fetch(`http://localhost:3000/${typeStorage}`, {
        method: 'POST',
        body: JSON.stringify(dataForm)
    });

    if (response.ok) {
        createNewTable(await response.json());
    } else {
        alert(`Ошибка HTTP: ${response.status}`);
    }
};

async function updateServe() {

    if (!checkInputsForNull()) {
        return false;
    }

    updateData();
    dataForm.id = inputId.value;
    
    let response = await fetch(`http://localhost:3000/${typeStorage}`, {
        method: 'PUT',
        body: JSON.stringify(dataForm)
    });

    if (response.ok) {
        createNewTable(await response.json());
    } else {
        alert(`Ошибка HTTP: ${response.status}`);
    }
};

async function deleteServe() {

    if (!checkInputsForNull()) {
        return false;
    }

    updateData();
    dataForm.id = inputId.value;
    
    let response = await fetch(`http://localhost:3000/${typeStorage}`, {
        method: 'DELETE',
        body: JSON.stringify(dataForm)
    });

    if (response.ok) {
        createNewTable(await response.json());
    } else {
        alert(`Ошибка HTTP: ${response.status}`);
    }
};

function createNewTable(data) {
    tableBody.innerHTML = null;
    
    for (let row of data) {
        let rowElement = () => {
            let tr = document.createElement("tr");
            tr.className = 'table__field';
            tr.id = `${row.id}`;
            tr.innerHTML = `
                    <td>${row.id}</td>
                    <td>${row.firstName}</td>
                    <td>${row.lastName}</td>
                    <td>${row.age}</td>`;
            return tr;
        };
        tableBody.append(rowElement());
    }
}

function selectRow(event) {
    const parentElement = event.target.parentElement;

    inputId.value = parentElement.firstElementChild.textContent;
    inputFirstName.value = parentElement.children[1].textContent;
    inputLastName.value = parentElement.children[2].textContent;
    inputAge.value = parentElement.children[3].textContent;
}

function updateData() {
    dataForm.firstName = inputFirstName.value;
    dataForm.lastName = inputLastName.value;
    dataForm.age = inputAge.value;
}

function clearInputs() {
    let inputs = document.querySelectorAll('input');

    for (let input of inputs) {
        input.value = '';
    }
}

function checkInputsForNull() {
    if (inputFirstName.value.trim() && inputLastName.value.trim() && inputAge.value.trim() !== '') {
        return true;
    }
    return false;
}