describe('Sinon test', () => {
    let sandbox;

    before(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    
    it('check that selectBtn called once function addRowToBegin', () => {
        const stub = sandbox.stub(window, 'addRowToBegin');
        const addStart = document.getElementById('add-start');

        addStart.click();

        sandbox.assert.calledOnce(stub);
    });
    
    it('check that selectBtn called once function addRowToMiddle', () => {
        const stub = sandbox.stub(window, 'addRowToMiddle');
        const addMiddle = document.getElementById('add-middle');

        addMiddle.click();

        sandbox.assert.calledOnce(stub);
    });
    
    it('check that selectBtn called once function addRowToEnd', () => {
        const stub = sandbox.stub(window, 'addRowToEnd');
        const addEnd = document.getElementById('add-end');

        addEnd.click();

        sandbox.assert.calledOnce(stub);
    });
    
    it('check that selectBtn called once function saveToLocal', () => {
        const stub = sandbox.stub(window, 'saveToLocal');
        const save = document.getElementById('save');

        save.click();

        sandbox.assert.calledOnce(stub);
    });
    
    it('check that selectBtn called once function readFromLocal', () => {
        const stub = sandbox.stub(window, 'readFromLocal');
        const read = document.getElementById('read');

        read.click();

        sandbox.assert.calledOnce(stub);
    });
    
});