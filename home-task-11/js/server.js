const http = require('http');
const port = 3000;
const mySqlBD = require('mysql2');
const {Client} = require('pg');
const fs = require('fs');

const users = [];
const fileJson = '../users-db.json';
let usersJson = [];
let storageName;

const mysqlConnect = mySqlBD.createConnection({
    host: "localhost",
    user: "root",
    database: "home",
    password: ""
});

const pg = new Client({
    host: "localhost",
    user: "postgres",
    database: "pg_base",
    password: "fhvfutljy"
});

pg.connect()
    .then( () => console.log(1))
    .catch(error => console.log(error))

const server = http.createServer(requestHandler);

function requestHandler(request, response) {
    response.setHeader('Access-Control-Allow-Origin', '*');
    response.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    response.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');
    response.writeHead(200, {"Content-Type": "application/json"});

    storageName = request.url.split('/')[1];

    readJsonFile();

    switch (request.method) {
        case 'GET':
            switch (storageName) {
                case 'Mock':
                    response.write(JSON.stringify(users));
                    response.end();
                    break;

                case 'JSON':
                    readJsonFile();

                    response.write(JSON.stringify(usersJson));
                    response.end();
                    break;

                case 'Select BD':
                    const nullStorage = [];
                    response.write(JSON.stringify(nullStorage));
                    response.end();
                    break;

                case 'mySql':
                    let sql =`SELECT * FROM sql_table`;

                    mysqlConnect.query(sql, async function (err, result) {
                        if (err){
                            console.error(err);
                        } else {
                            response.write(await JSON.stringify(result));
                            response.end();
                        }
                    });
                    break;
            }
        break;

        case 'POST':
            switch (storageName) {
                case 'Mock':
                    let body = '';

                    request.on('data', async chunk => {
                        body += await chunk.toString();
                    });
                    request.on('end', () => mockResponsePost(response, body));
                    break;

                case 'JSON':   
                    let bodyJson = '';

                    request.on('data', async chunk => {
                        bodyJson += await chunk.toString();
                    });
                    request.on('end', () => jsonResponsePost(response, bodyJson));
                    break;

                case 'mySql':
                    let bodySql = '';

                    request.on('data', async chunk => {
                        bodySql += await chunk.toString();
                    });
                    request.on('end', () => {
                        bodySql = JSON.parse(bodySql);

                        let sql =`INSERT INTO sql_table(firstName, lastName, age) VALUES("${bodySql.firstName}","${bodySql.lastName}","${bodySql.age}")`;

                        mysqlConnect.query(sql,  async function (err, result) {
                            if (err){
                                console.error(err);
                            } else {
                                let sqlGet =`SELECT * FROM sql_table`;

                                mysqlConnect.query(sqlGet, async function (err, result) {
                                    if (err){
                                        console.error(err);
                                    } else {
                                        response.write(await JSON.stringify(result));
                                        response.end();
                                    }
                                });
                            }
                        });
                    });
                    break;
            }
        break;

        case 'PUT':
            switch (storageName) {
                case 'Mock':
                    let put = '';

                    request.on('data', async chunk => {
                        put += await chunk.toString();
                    });
                    request.on('end', () => mockResponsePut(response, put));
                    break;

                case 'JSON':
                    let putJson = '';

                    request.on('data', async chunk => {
                        putJson += await chunk.toString();
                    });
                    request.on('end', () => jsonResponsePut(response, putJson));       
                    break;

                case 'mySql':
                    let putSql = '';

                    request.on('data', async chunk => {
                        putSql += await chunk.toString();
                    });

                    request.on('end', () => {
                        putSql = JSON.parse(putSql);

                        let sql =`UPDATE sql_table SET firstName="${putSql.firstName}", lastName="${putSql.lastName}", age="${putSql.age}" WHERE id=${putSql.id}`;

                        mysqlConnect.query(sql, async function (err, result) {
                            if (err){
                                console.error(err);
                            } else {
                                let sqlGet =`SELECT * FROM sql_table`;

                                mysqlConnect.query(sqlGet, async function (err, result) {
                                    if (err){
                                        console.error(err);
                                    } else {
                                        response.write(await JSON.stringify(result));
                                        response.end();
                                    }
                                });
                            }
                        });
                    });
                    break;
            }
        break; 

        case 'DELETE':

            switch (storageName) {
                case 'Mock':
                    let del = '';

                    request.on('data', async chunk => {
                        del += await chunk.toString();
                    });        
                    request.on('end', () => mockResponseDelete(response, del));
                    break;

                case 'JSON':
                    let delJson = '';

                    request.on('data', async chunk => {
                        delJson += await chunk.toString();
                    });        
                    request.on('end', () => jsonResponseDelete(response, delJson));
                    break;

                case 'mySql':
                    
                    let deleteSql = '';

                    request.on('data', async chunk => {
                        deleteSql += await chunk.toString();
                    });

                    request.on('end', () => {
                        deleteSql = JSON.parse(deleteSql);

                        let sql =`DELETE FROM sql_table WHERE id=${deleteSql.id}`;

                        mysqlConnect.query(sql, async function (err, result) {
                            if (err){
                                console.error(err);
                            } else {
                                let sqlGet =`SELECT * FROM sql_table`;

                                mysqlConnect.query(sqlGet, async function (err, result) {
                                    if (err){
                                        console.error(err);
                                    } else {
                                        response.write(await JSON.stringify(result));
                                        response.end();
                                    }
                                });
                            }
                        });
                    });
                    break;
            }
        break;
            
        case 'OPTIONS':
            response.end();
            break;
    }

}

async function mockResponsePost(response, body) {
    body = await JSON.parse(body);
    body.id = Math.floor(Math.random() * 1000000);

    users.push(body);
    response.end(JSON.stringify(users));
}

async function jsonResponsePost(response, body) {
    readJsonFile();
    
    body = await JSON.parse(body);
    body.id = Math.floor(Math.random() * 1000000);

    usersJson.push(body);
    response.end(JSON.stringify(usersJson));
    saveJsonFile();
}

async function mockResponsePut(response, put) {
    put = await JSON.parse(put);

    let changeItem = users.findIndex(item => item.id == put.id);
    users.splice(changeItem, 1, put);

    response.end(JSON.stringify(users));
}

async function jsonResponsePut(response, put) {
    readJsonFile();
    put = await JSON.parse(put);

    let changeItem = usersJson.findIndex(item => item.id == put.id);
    usersJson.splice(changeItem, 1, put);

    response.end(JSON.stringify(usersJson));
    saveJsonFile();
}

async function mockResponseDelete(response, del) {
    readJsonFile();
    del = await JSON.parse(del);

    let deleteItem = users.findIndex(item => item.id == del.id);
    users.splice(deleteItem, 1);

    response.end(JSON.stringify(users));
    saveJsonFile();
}

async function jsonResponseDelete(response, del) {
    readJsonFile();
    del = await JSON.parse(del);

    let deleteItem = usersJson.findIndex(item => item.id == del.id);
    usersJson.splice(deleteItem, 1);

    response.end(JSON.stringify(usersJson));
    saveJsonFile();
}

function readJsonFile() {
    fs.readFile(fileJson, 'utf8', (err, data) => {
        if (!err) {
            if (data) {
                usersJson = JSON.parse(data);
            }
        } else {
            console.error(err);
        }
    })
}

function saveJsonFile() {
    fs.writeFile(fileJson, JSON.stringify(usersJson), (err) => {
        if(err) throw err;
    })
}

server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})
