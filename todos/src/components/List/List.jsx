import React from 'react';
import { ListItem } from './ListItem.jsx'

export class List extends React.Component {
    render() {

        const todos = this.props.todos;
        const renderItems = todos.map(elem => 
            <ListItem
                value={elem.value}
                id={elem.id.toString()}
                key={elem.id.toString()}
                handleClickDel={this.props.handleClickDel}
            />
        );
        return (
            <ul className="list">
                {renderItems}
            </ul>
        )
    }
}