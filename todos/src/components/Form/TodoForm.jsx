import React from 'react';
export class Form extends React.Component {
    render() {
        return (
            <form className="form" onSubmit={this.props.handleSubmit}>
                <input type="text" className="new-task" required value={this.props.value} onChange={this.props.handleChange}/>
                <input type="submit" className="add-btn" value="Add"/>
            </form>
        )
    }
}