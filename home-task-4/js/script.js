(function (){
    const activeBtn = document.getElementById('main-btn');
    const parsedContent = document.getElementById('parsed-content');
    let nestingCounter;


    activeBtn.addEventListener('click', showNestingWrapper);
    parsedContent.addEventListener('click', showNesting);
    activeBtn.addEventListener('click', setBackgroundNesting);


    function showNestingWrapper() {
        const defultText = document.getElementById('main-textarea');
        const parsedValue = JSON.parse(defultText.value);
        const addContent = document.getElementById('add-content');
  
        addContent.innerHTML = showItems(parsedValue);
        parsedContent.style.visibility = 'visible';
        addContent.classList.add('nesting-values');
    }

    function showItems(obj) {
        let result = '';

        for (let key in obj) {

            if (typeof obj[key] === 'object') {
                result += `<div class="nesting-style">
                    <div class="nesting-title__block">
                    <span class="nesting-pointer" data-action="rotate">&#x25bc;</span>
                    <span class="nesting-title">${key}</span></div> <div class="nesting-bg-style nesting-values">${showItems(obj[key])}</div></div>`
                    nestingCounter++;
            } else {
                result += `<p>${key} : ${obj[key]}</p>`;
            }
        }
        return result;
    }

    function showNesting(event) {
        const target = event.target;
        
        if (!target.hasAttribute('data-action')) {
            return;
        }

        if (target.hasAttribute('data-action') && target.getAttribute('style') === null) {
            target.setAttribute('style', 'transform: rotate(-180deg)');
            target.parentElement.nextElementSibling.classList.remove('nesting-values');
        } else {
            target.setAttribute('style', 'display: block, transform: rotate(0deg)');
            target.parentElement.nextElementSibling.classList.add('nesting-values');
            target.removeAttribute('style');
        }
    }

    function setBackgroundNesting() {
        const nestingStyle = [...document.getElementsByClassName('nesting-bg-style')];

        for (let i = 0; i < nestingStyle.length; i+=counter) {
        
            if (nestingStyle[i].firstElementChild.tagName === 'P') {
                continue;
            }
            
            for (let j = 0; j < nestingStyle[i].children.length; j++) {
                if (nestingStyle[i].children[j].tagName === 'P') {
                    continue;
                }
                nestingStyle[i].children[j].classList.add('nesting-bg-orange');
            }
        }
    }
})();
