const WebSocketServer = new require('ws');

let users = {};

function User(){
    this.connect = null;
    this.id = null;
    this.name = null;
    this.chat = 'chat1';
    this.status = 'offline';
}


const webSocketServer = new WebSocketServer.Server({port: 3000});

webSocketServer.on('connection', function(ws) {
    const id = Math.floor(Math.random() * 1000000);  
    users[id] = new User;
    users[id].connect = ws;  
    users[id].chat = 'chat1';
    users[id].id = id;

    ws.on('message', function(message) {
        let newMessage = JSON.parse(message);
           
        if (!users[id].name) {
            users[id].name = newMessage.userName;
        }
        console.log(newMessage.msgType);
        switch (newMessage.msgType) {
            case 'login':
                for (let key in users) {

                    if (users[key].name === users[id].name && users[key].status !== users[id].status) {
        
                        users[id].connect.send(JSON.stringify({msgType: 'userOnConnect',}));
        
                    } else if (users[key].name === users[id].name && users[key].status === 'offline') {

                        users[id].connect.send(JSON.stringify({msgType: 'userOffConnect',}));
                    }
                }
                users[id].status = 'online';
                break;
            case 'message':
                    let msgToChat = JSON.stringify({
                        messageText: newMessage.messageText,
                        userName: users[id].name,
                        userID: users[id].id,
                        chat: newMessage.chat,
                        msgType: 'chatMessage',
                    });
                    
                    for (let key in users) {   
                        users[key].connect.send(msgToChat);
                    }
            
                    let brigtUser = JSON.stringify({
                        userName: users[id].name,
                        chat: newMessage.chat,
                        msgType: 'userHighlight',
                    });
            
                    users[id].connect.send(brigtUser);
                break;
            default:
                break;
        } 
    });


    ws.on('close', function() {
        const allChat = ['chat1', 'chat2', 'chat3'];

        users[id].status = 'offline';

        for (let chat of allChat) {
            let endConnect = JSON.stringify({userName: users[id].name, chat: chat, msgType: 'userOffline',});
            for (let key in users) {
                users[key].connect.send(endConnect);
            }
        }
    });
});
