describe('Testing', function() {

    const aList = new Alist();

    describe('check aList method .push()', function() {

        it (`store = [], store.length = 0, set undefined, expected store.length = 1`, function() {
            const params = undefined;
            aList.store = [];
            const actual = aList.push(params);
            const expected = 1;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 10], store.length = 2, set string, expected store.length = 3`, function() {
            const params = 'Hellow Pete';
            aList.store = [2, 10];
            const actual = aList.push(params);
            const expected = 3;
            assert.strictEqual(actual, expected);
        });

        it (`store = ['Hellow Pete'], store.length = 1, set array, expected store.length = 2`, function() {
            const params = [2, 6, 9];
            aList.store = ['Hellow Pete'];
            const actual = aList.push(params);
            const expected = 2;
            assert.strictEqual(actual, expected);
        });

        it (`store = [4, 7, 9], store.length = 3, set Object, expected store.length = 4`, function() {
            const params = {name: 'Vasy', surname: 'Ivanov'};
            aList.store = [4, 7, 9];
            const actual = aList.push(params);
            const expected = 4;
            assert.strictEqual(actual, expected);
        });

        it (`store = [], store.length = 0, set nothing, expected store.length = 0`, function() {
            aList.store = [];
            const actual = aList.push();
            const expected = 0;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 4, 7], store.length = 3, set nothing, expected store.length = 3`, function() {
            aList.store = [2, 4, 7];
            const actual = aList.push();
            const expected = 3;
            assert.strictEqual(actual, expected);
        });
    });

    describe('check aList method .pop()', function() {

        it (`store = [], expected undefined`, function() {
            aList.store = [];
            const actual = aList.pop();
            const expected = undefined;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 10], expected last param = 10`, function() {
            aList.store = [2, 10];
            const actual = aList.pop();
            const expected = 10;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 6, 9, [1, 2]], expected last param = [1, 2]`, function() {
            aList.store = [2, 6, 9, [1, 2]];
            const actual = aList.pop();
            const expected = [1, 2];
            assert.deepEqual(actual, expected);
        });

        it (`store = [4, 7, 9], set array, call the method with params [5, 10], expected 9`, function() {
            const params = [5, 10];
            aList.store = [4, 7, 9];
            const actual = aList.pop(params);
            const expected = 9;
            assert.strictEqual(actual, expected);
        });
    });

    describe('check aList method .shift()', function() {

        it (`store = [], expected undefined`, function() {
            aList.store = [];
            const actual = aList.shift();
            const expected = undefined;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 10], expected first param = 2`, function() {
            aList.store = [2, 10];
            const actual = aList.shift();
            const expected = 2;
            assert.strictEqual(actual, expected);
        });

        it (`store = [[1, 2], 2, 6, 9], expected first param = [1, 2]`, function() {
            aList.store = [[1, 2], 2, 6, 9];
            const actual = aList.shift();
            const expected = [1, 2];
            assert.deepEqual(actual, expected);
        });

        it (`store = [4, 7, 9], set array, call the method with params [5, 10], expected first param = 4`, function() {
            const params = [5, 10];
            aList.store = [4, 7, 9];
            const actual = aList.shift(params);
            const expected = 4;
            assert.strictEqual(actual, expected);
        });
    });

    describe('check aList method .unshift()', function() {

        it (`store = [], store.length = 0, set (2, 6, ['hellow']), expected store.length = 3`, function() {
            aList.store = [];
            const actual = aList.unshift(2, 6, ['hellow']);
            const expected = 3;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 10], store.length = 2, set string, expected store.length = 3`, function() {
            const params = 'Hellow Pete';
            aList.store = [2, 10];
            const actual = aList.unshift(params);
            const expected = 3;
            assert.strictEqual(actual, expected);
        });

        it (`store = ['Hellow Pete'], store.length = 1, set array, expected store.length = 2`, function() {
            const params = [2, 6, 9];
            aList.store = ['Hellow Pete'];
            const actual = aList.unshift(params);
            const expected = 2;
            assert.strictEqual(actual, expected);
        });

        it (`store = [4, 7, 9], store.length = 3, set Object, expected store.length = 4`, function() {
            const params = {name: 'Vasy', surname: 'Ivanov'};
            aList.store = [4, 7, 9];
            const actual = aList.unshift(params);
            const expected = 4;
            assert.strictEqual(actual, expected);
        });

        it (`store = [], store.length = 0, set nothing, expected store.length = 0`, function() {
            aList.store = [];
            const actual = aList.unshift();
            const expected = 0;
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 4, 7], store.length = 3, set nothing, expected store.length = 3`, function() {
            aList.store = [2, 4, 7];
            const actual = aList.unshift();
            const expected = 3;
            assert.strictEqual(actual, expected);
        });
    });

    describe('check aList method .toString()', function() {

        it (`store = [2, 10], expected string = '2, 10'`, function() {
            aList.store = [2, 10];
            const actual = aList.toString();
            const expected = '2,10';
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 10, ['hellow']], expected string = '2, 10, hellow'`, function() {
            aList.store = [2, 10, ['hellow']];
            const actual = aList.toString();
            const expected = '2,10,hellow';
            assert.strictEqual(actual, expected);
        });

        it (`store = [{name: 'pete', surname: 'Ivanov'}, 2, 6, 9], expected string = '[object Object], 2, 6, 9'`, function() {
            aList.store = [{name: 'pete', surname: 'Ivanov'}, 2, 6, 9];
            const actual = aList.toString();
            const expected = '[object Object],2,6,9';
            assert.strictEqual(actual, expected);
        });

        it (`store = [], expected string = ''`, function() {
            aList.store = [];
            const actual = aList.toString();
            const expected = '';
            assert.strictEqual(actual, expected);
        });

        it (`store = [2, 6, 9], set param = 5, expected string = '2,6,9'`, function() {
            const params = 5;
            aList.store = [2, 6, 9];
            const actual = aList.toString(params);
            const expected = '2,6,9';
            assert.strictEqual(actual, expected);
        });
    });

    describe('check aList method .sort()', function() {
        
        it (`store = [], expected array = []`, function() {
            aList.store = [];
            const actual = aList.sort();
            const expected = [];
            assert.deepEqual(actual, expected);
        });

        it (`store = [2], expected return arr = [2]`, function() {
            aList.store = [2];
            const actual = aList.sort();
            const expected = [2];
            assert.deepEqual(actual, expected);
        });

        it (`store = [30, 5, 7, 10], expected sort array as number = [5, 7, 10, 30]`, function() {
            aList.store = [30, 5, 7, 10];
            const actual = aList.sort();
            const expected = [5, 7, 10, 30];
            assert.deepEqual(actual, expected);
        });

        it (`store = ['30', '5', '7', '10'], expected sort array as string = ['10', '30', '5', '7']`, function() {
            aList.store = ['30', '5', '7', '10'];
            const actual = aList.sort();
            const expected = ['10', '30', '5', '7'];
            assert.deepEqual(actual, expected);
        });
    });

    const list = new List();

    describe('check list method .push()', function() {

        it (`store = [], store.length = 0, set undefined, expected store.length = 1`, function() {
            const params = undefined;
            list.store = [];
            const actual = list.push(params);
            const expected = 1;
            assert.strictEqual(actual, expected);
        });
    });
});
