// const person = {
//     _name: 'Stas',
//     _age: 26,
//     _gender: 'male',
//     _skills: ['java', 'js'],
//     get name() {
//         return this._name;
//     },
//     set name (value) {
//         this._name = value;
//     },
//     get age() {
//         return this._age;
//     },
//     set age(value) {
//         if (value > 0 && value < 200) {
//            this._age = value;
//         } else {
//             console.log('неправельный возраст');
//         }
//     },
//     set skills(value) {
//         if(!this._skills.find(skill => skill === value)) {
//             this._skills = [...this._skills, value];
//         }
//     },
//     get gender() {
//         return this._gender;
//     },
//     set gender(value) {
//         Object.defineProperty(person, '_gender', {
//             value: value,
//             writable: false,
//             enumerable: true,
//             configurable: true,
//         });
//     }
// }

// console.log(Object.getOwnPropertyDescriptor(person, 'friend'));


// person._gender = 'female';

// Object.defineProperty(person, 'gender', {
//     value: 'female',
//     writable: true,
//     enumerable: true,
//     configurable: true,
// });

// Object.defineProperties(person, {
//     _age: {
//         value: 26,
//         writable: false,
//         enumerable: false,
//         configurable: true,
//     },
//     age: {
//         set(value) {
//             Object.defineProperty(person, '_age', {
//                 value
//             })
//         },
//         get() {
//             return this._age;
//         }
//     }
// });

// person._age = 4534534;
// console.log(person.age);
// person.age = 2;
// console.log(person.age);


// person.family = 'dfgdfgdf';
// console.log('person',person);
// Object.preventExtensions(person);
// person.add = '';
// console.log('person',person);
// person.friend = 'dfdg';
// console.log(person.friend);
// console.log(Object.keys(person));
// person.age = 150;
// person.skills = 'java';
// person.skills = 'java';
// person.skills = 'js';
// console.log(person._skills);



// let skills = ['js', 'jsvs', 'C#'];

// for (const skill of skills) {
//     console.log('skill', skill);
// }
// console.log(skills[Symbol.iterator]);
// let iterator = skills[Symbol.iterator]();
// console.log(iterator.next().value);
// console.log(iterator.next());
// console.log(iterator.next());
// console.log(iterator.next());


// const myGenerator = {
//     [Symbol.iterator]() {
//         let index = 1;
//         return {
//             next() {
//                 let value = index > 50 ? undefined : Math.floor(Math.random() * 100);
//                 let done = !value;
//                 return {value, done};
//             }
//         }
//     }
// }

// for (const gen of myGenerator) {
//     console.log(gen);
// }

// function* generate() {
//     console.log('start');
//     yield;
//     console.log('End');
// }

// let iter = generate();
// iter.next();
// iter.next();

// function* generate() {
//     yield 1;
//     yield 2;
//     yield 3;
// }

// let iter = generate();
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next().value);
// console.log(iter.next());

function* generate() {
    const res = 5 + (yield);
    yield console.log(res);
}

let iter = generate();
iter.next();
console.log(iter.next(3));
console.log(iter.next());