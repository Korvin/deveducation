describe('Test', function() {    
    describe('Тест функции addPoint', function() {
        it (`передаем 555.23 с точкой, ожидаем тоже чило 555.23`, function() {
            const str = '555.23';
            const actual = addPoint(str);
            const expected = 555.23;

            assert.equal(actual, expected);
        });
        it (`передаем 555 ожидаем добавление в конец точки 555.`, function() {
            const str = '555';
            const actual = addPoint(str);
            const expected = 555.;
            
            assert.equal(actual, expected);
        });
    });

    describe('Тест функции countResult', function() {
        it (`передаем 5 + 10, ожидаем сложение 15`, function() {
            const sign = '+';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = 15;

            assert.equal(actual, expected);
        });
        it (`передаем 5 - 10, ожидаем вычитание -5`, function() {
            const sign = '-';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = -5;

            assert.equal(actual, expected);
        });
        it (`передаем 5 * 10, ожидаем умножение 50`, function() {
            const sign = '*';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = 50;

            assert.equal(actual, expected);
        });
        it (`передаем 5 / 10, ожидаем деление 0.5`, function() {
            const sign = '/';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = 0.5;

            assert.equal(actual, expected);
        });
        it (`передаем 5 = 10, ожидаем undefined`, function() {
            const sign = '=';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = undefined;

            assert.equal(actual, expected);
        });
        it (`передаем 5 . 10, ожидаем undefined`, function() {
            const sign = '=';
            const firstValue = 5;
            const secondValue = 10;
            const actual = countResult(sign, firstValue, secondValue);
            const expected = undefined;

            assert.equal(actual, expected);
        });
    });

    describe('Тест функции deleteLastNumber', function() {
        it (`передаем '23.55' ожидаем удаление последнего символа '23.5'`, function() {
            const str = '23.55';
            const actual = deleteLastNumber(str);
            const expected = '23.5';

            assert.strictEqual(actual, expected);
        });
        it (`передаем '555.' ожидаем удаление последнего символа '555'`, function() {
            const str = '555.';
            const actual = deleteLastNumber(str);
            const expected = '555';
            
            assert.strictEqual(actual, expected);
        });
        it (`передаем пустую строку '' ожидаем пустую строку ''`, function() {
            const str = '';
            const actual = deleteLastNumber(str);
            const expected = '';
            
            assert.strictEqual(actual, expected);
        });
    });
	
	describe('Sinon test', () => {
        let sandbox;
    
        before(() => {
            sandbox = sinon.createSandbox();
        });

        afterEach(() => {
            sandbox.restore();
        });
    
        
        it('call function addNumber with value "7"', () => {
            const stub = sandbox.stub(window, 'addNumber');
            const btnNumberSeven = document.querySelector('#seven');
    
            btnNumberSeven.click();
    
            sandbox.assert.calledOnce(stub);
            sandbox.assert.calledWith(stub, '7');
        });
        it('call function addNumber with values "7" and "8" and second call was "7"', () => {
            const stub = sandbox.stub(window, 'addNumber');
            const btnNumberSeven = document.querySelector('#seven');
            const btnNumberEight = document.querySelector('#eight');

            btnNumberEight.click();
            btnNumberSeven.click();
    
            sandbox.assert.calledTwice(stub);
            sandbox.assert.calledWith(stub.secondCall, '7');
        });
        it('call function addSign and check its work on click "+"', () => {
            const stub = sandbox.stub(window, 'addSign');
            const btnSignPlus = document.querySelector('#plus');

            btnSignPlus.click();
    
            sandbox.assert.calledOnce(stub);
            sandbox.assert.calledWith(stub, '+');
        });
        it('call function addSign and check its work on click "+" and "-", and check first call "+"', () => {
            const stub = sandbox.stub(window, 'addSign');
            const btnSignPlus = document.querySelector('#plus');
            const btnSignMinus = document.querySelector('#minus');

            btnSignPlus.click();
            btnSignMinus.click();
    
            sandbox.assert.calledTwice(stub);
            sandbox.assert.calledWith(stub.firstCall, '+');
        });
    });
});