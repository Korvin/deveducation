let sign;
let firstValue;
let secondValue;
let refreshInput = false;
let result;

const inputScreen = document.getElementById('input-screen');
const btnArea = document.getElementById('btn-area');


btnArea.addEventListener('click', selectButton);

function selectButton(event) {
    const buttonType = event.target.dataset.buttonType;
    if (!buttonType) {
        return;
    }
    switch(buttonType) {
        case 'clear':
            cleanCalc();
            break;
        case 'delete':
            inputScreen.value = deleteLastNumber(inputScreen.value);
            break;
        case 'equal':
            countNumbers();
            break;
        case 'point':
            addPoint(inputScreen.value);
            break;
        case 'sign':
            addSign(event.target.value);
            break;
        case 'number':
            addNumber(event.target.value);
            break;
    }
}

function cleanCalc() {
    sign = undefined;
    firstValue = undefined;
    secondValue = undefined;
    result = undefined;
    refreshInput = false;
    inputScreen.value = '';
}

function deleteLastNumber(str) {
    return str.substring(0, str.length - 1);
}

function addPoint(str) {
    if (!str.includes('.')) {
        return str += '.';
    }
    return str;
}

function addNumber(number) {
    if (refreshInput) {
        inputScreen.value = '';
    }
    refreshInput = false;
    inputScreen.value += number; 
}

function setFirstValue() {
    if (!isNaN(inputScreen.value)) {
        firstValue = inputScreen.value;
    }
}

function setSecondValue() {
    if (firstValue !== undefined) {
        secondValue = inputScreen.value;
    }
}

function addSign(operation) {
    if (sign && !refreshInput) {
        setSecondValue();
        countNumbers();
    } else {
        setFirstValue();
    }
    sign = operation;
    refreshInput = true;
}

function countNumbers() {
    if (secondValue === undefined || !refreshInput) {
        secondValue = inputScreen.value;
    }

    if (result !== undefined) {
        firstValue = result;
    }
    result = countResult(sign, firstValue, secondValue);
    refreshInput = true;
    inputScreen.value = result;
}

function countResult(operation, a, b) {
    let result;
    switch(operation) {
        case '+':
            result = Number(a) + Number(b);
            break;
        case '-':
            result = Number(a) - Number(b);
            break;
        case '*':
            result = Number(a) * Number(b);
            break;
        case '/':
            result = Number(a) / Number(b);
            break;
        default: 
            return undefined;
    }
    return result;
}
