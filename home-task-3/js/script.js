class Alist {
    store = [];

    push(...rest) {
        this.store = [...this.store, ...rest];
        return this.store.length;
    };

    pop() {
        if (this.store.length !== 0) {
            const lastItem = this.store[this.store.length - 1];

            this.store.length = this.store.length - 1;
            return lastItem;
        }
        return undefined;
    }

    shift() {
        if (this.store.length !== 0) {
            const [firstItem, ...rest] = this.store;

            this.store = [...rest];
            return firstItem;
        }
        return undefined;
    }

    unshift(...rest) {
        this.store = [...rest, ...this.store];
        return this.store.length;
    }

    toString() {
        let result = '';
        for (let i = 0; i < this.store.length; i++) {
            if (i < this.store.length - 1) {
                result += this.store[i] + ',';
            } else {
                result += this.store[i];
            }
        }
        return result;
    }

    defultSortFunc = (a, b) => {
        if (typeof a === 'number' && typeof b === 'number') {
            return a - b;
        } else {
            if (a < b) {
                return 1;
            }

            if (a > b) {
                return -1;
            }

            if (a === b) {
                return 0;
            }
        }
    }

    sort(someFunc = this.defultSortFunc) {

        const data = this.store;
        let i = 0;  
        const sortFunc = someFunc(data[i], data[i + 1]);

        if (data.length <= 1) {
            return data;
        }

        if (sortFunc > 0) {
            for (; i < data.length - 1; i++) {
                for (let j = i + 1; j < data.length; j++) {
                    if (data[i] > data[j]) {
                        let elem = data[j];
                        data[j] = data[i]
                        data[i] = elem;
                    }
                }
            }
            return data;
        } else {
            for (; i < data.length - 1; i++) {
                for (let j = i + 1; j < data.length; j++) {
                    if (data[i] < data[j]) {
                        let elem = data[j];
                        data[j] = data[i]
                        data[i] = elem;
                    }
                }
            }
            return data;
        }   
    }
}

const arr = new Alist();

console.log(arr.push([5, 3], [3, 5], [1, 1]));
console.log(arr.sort());


class List {
    root = null;
    length = 0;

    setRoot(value) {
        if (!this.root) {
            this.length++;
            this.root = this.createNode(value);
            return true;
        }
        return false;
    }

    createNode(value) {
        return {
            value,
            next: null,
        }
    }

    push(...values) {
        let starter = 0;

        if (values.length === 0) {
            return this.length;
        }

        if (this.root === null) {
            this.setRoot(values[0]);

            starter = 1;

            if (values.length === 1) {
                return this.length++;
            }
        }

        for (let j = starter; j < values.length; j++) {
            let pointer = this.root;
            for (let i = 0; i < this.length - 1; i++) {
                pointer = pointer.next;
            }
            pointer.next = this.createNode(values);

            this.length++;
        }

        return this.length;
    }
}

const list = new List();

console.log(list.push(3, 4, 5, [5,3,7]));
console.log(list.push(1));


// let pointer = list.root;
// for (let i = 0; i < list.length; i++) {
//     console.log(pointer);
//     pointer = pointer.next;
// }

