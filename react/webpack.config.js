const path = require('path');

module.exports = {
    entry: {
        app: './src/index.js'
    },
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, './dist'),
    },
    module: {
        rules: [
            {
                test: /\.(js.jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            }
        ]
    }
}