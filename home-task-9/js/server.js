const WebSocketServer = new require('ws');

let users = {};

function User(){
    this.connect = null;
    this.id = null;
    this.name = null;
    this.chat = 'chat1';
}


const webSocketServer = new WebSocketServer.Server({port: 3000});

webSocketServer.on('connection', function(ws) {
    const id = Math.floor(Math.random() * 1000000);
    
    users[id] = new User;
    users[id].connect = ws;
    users[id].id = id;
    users[id].chat = 'chat1';

    ws.on('message', function(message) {
        
        let newMessage = JSON.parse(message);
        
        
        if (!users[id].name) {
            users[id].name = newMessage.userName;
        }

        let msgToChat = JSON.stringify({
            messageText: newMessage.messageText,
            userName: users[id].name,
            userID: users[id].id,
            chat: newMessage.chat,
            msgType: 'chatMessage',
        });
        
        for (let key in users) {   
            users[key].connect.send(msgToChat);
        }
    });


    ws.on('close', function() {
        const allChat = ['chat1', 'chat2', 'chat3'];
        for (let chat of allChat) {
            let endConnect = JSON.stringify({userName: users[id].name, chat: chat, msgType: 'userOffline',});
            for (let key in users) {
                users[key].connect.send(endConnect);
            }
        }
        
    });
});
