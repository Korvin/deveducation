import React from "react";
import "./styles/style.css";
import axios from "axios";
import { LoginPage } from "./LoginPage/LoginPage.jsx";
import { MainPage } from "./MainPage/MainPage.jsx";

export class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      userName: "Test",
      editData: null,
      loginData: {
        user: "admin",
        dbName: "db_tables",
        password: "root",
        tableName: "person",
        isLoggedSuccessfully: false
      },
      selectedTab: "logs", // edit
      currentVersion: "Version1",
      userPermissions: {
        read: false,
        write: false,
        create: false
      },
      errorData: {
        isError: false,
        errorMessage: '',
      }
    };
  }
  onChangeLoginInput = e => {
    const target = e.target;
    const value = target.value;
    const name = target.name;
    this.setState({ loginData: { ...this.state.loginData, [name]: value } });
  };

  onSubmitLoginPage = e => {
    e.preventDefault();
    this.connectDB(this.state.loginData);
  };

  onLogoutClick = e => {
    this.disconnect();
  }

  disconnect = () => {
    axios
      .post("http://localhost:8080", {
        title: "Disconnect",
        body: JSON.stringify('')
      })
      .then(response => {
        console.log("response Disconnect", response);
        this.setState({ loginData: { ...this.state.loginData, isLoggedSuccessfully: false } })
      })
      .catch(error => {
        console.log("error", error.response);
      });
  };

  connectDB = data => {
    console.log(data);
    axios
      .post("http://localhost:8080", {
        title: "Connect",
        body: JSON.stringify(data)
      })
      .then(response => {
        console.log("response", response);
        this.successConnect();
      })
      .catch(error => {
        this.showLoginError(error.response);
      });
  };

  successConnect = () => {
    const errorData = Object.assign({}, this.state.errorData);
    const loginData = Object.assign({}, this.state.loginData);
    errorData.isError = false;
    errorData.errorMessage = '';
    loginData.isLoggedSuccessfully = true;

    this.setState(state => {
        return {
            errorData: errorData,
            loginData: loginData,
        }
    })
};

showLoginError = response => {
    const serverErrorMessage = response.data.split('\n')[0];
    const errorData = Object.assign({}, this.state.errorData);
    errorData.isError = true;
    errorData.errorMessage = serverErrorMessage;

    this.setState(state => {
        return {
            errorData: errorData,
        }
    });

    setTimeout(() => {
        errorData.isError = false;
        errorData.errorMessage = '';
        this.setState(state => {
            return {
                errorData: errorData,
            }
        })
    }, 3000)
};

  render() {
    return (
      <div>
        <div className="wrapper">
          {this.state.loginData.isLoggedSuccessfully ? 

            <MainPage
              onLogoutClick={this.onLogoutClick}
            /> :

            <LoginPage
              user={this.state.loginData.user}
              dbName={this.state.loginData.dbName}
              password={this.state.loginData.password}
              tableName={this.state.loginData.tableName}
              isError={this.state.errorData.isError}
              errorMessage={this.state.errorData.errorMessage}
              onChangeLoginInput={this.onChangeLoginInput}
              onSubmitLoginPage={this.onSubmitLoginPage}
            />
          }
        </div>
      </div>
    );
  }
}
