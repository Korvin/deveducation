import React, { Component } from "react";
import { Header } from "./Header.jsx";

export class MainPage extends Component {
    render() {
        return (
            <div className="wrapper__main-container main-container" id="mainContainer">
                <Header
                    onLogoutClick={this.props.onLogoutClick}
                />
            </div>
        );
    }
}