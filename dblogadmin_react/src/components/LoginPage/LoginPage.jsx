import React, { Component } from "react";

export class LoginPage extends Component {
  render() {
    return (
      <div className="container form">
        <form
          name="loginForm"
          className="form__input"
          onSubmit={this.props.onSubmitLoginPage}
        >
          <div className="form__input-item">
            <label>
              DB:
              <input
                className="input"
                type="text"
                name="dbName"
                value={this.props.dbName}
                onChange={this.props.onChangeLoginInput}
              />
            </label>
          </div>

          <div className="form__input-item">
            <label>
              Table:
              <input
                className="input"
                type="text"
                name="tableName"
                value={this.props.tableName}
                onChange={this.props.onChangeLoginInput}
              />
            </label>
          </div>

          <div className="form__input-item">
            <label>
              Login:
              <input
                className="input"
                type="text"
                name="user"
                value={this.props.user}
                onChange={this.props.onChangeLoginInput}
              />
            </label>
          </div>

          <div className="form__input-item">
            <label>
              Pass:
              <input
                className="input"
                type="password"
                name="password"
                value={this.props.password}
                onChange={this.props.onChangeLoginInput}
              />
            </label>
          </div>

          <div className="form__button">
            <input className="form__button-item" type="submit" value="Submit" />
          </div>

          <p
              className="form__warning"
              style={this.props.isError ? {visibility: 'visible'} : {visibility: 'hidden'}}
          >
              {this.props.errorMessage}
          </p>
        </form>
      </div>
    );
  }
}
