const http = require("http");
const url = require("url");
const mysql = require("mysql2");
const { searchValue } = require("./config");
const { sendEmails } = require("./sendEmails");
let connection;
let oldData = null;
let mainUser = null;

http
  .createServer(async (request, response) => {
    const pathName = url.parse(request.url, true).pathname;
    const [, version, db_table, id] = pathName.split("/");
    let logsId = [];

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PUT, DELETE"
    );
    response.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept"
    );
    response.setHeader(
      "Content-Type",
      "application/json, text/plain; charset=utf-8;"
    );
    response.setHeader("Access-Control-Max-Age", "-1");
    response.statusCode = 200;

    if (request.method === "POST") {
      const data = await getBody(request);
      if (data.title == "Connect") {
        try {
          const connection = await connectDB(JSON.parse(data.body));
          console.log("success", connection);
          response.write(JSON.stringify(connection));
          response.end();
        } catch (err) {
          console.log("errrrr" + err);
          response.statusCode = 500;
          response.write(JSON.stringify(err));
          response.end();
        }
      } else if (data.title == "Disconnect") {
        disconnectDB();
        response.write(JSON.stringify("Connection is closed"));
        response.end();
      }
    }

    if (request.method === "GET") {
      let result;
      if (db_table === "person") {
        if (id) {
          result = await readPersonData(version, id);
        } else {
          result = await readList(version);
        }
      }
      if (db_table === "person_logs") {
        result = await readLogsData(version);
      }
      response.write(JSON.stringify(result));
      response.end();
    }

    if (request.method === "PUT") {
      let body = await getBody(request);
      const result = await updatePersonData(version, JSON.stringify(body), id);
      response.write(JSON.stringify(result));
      response.end();
      const changeArr = changesInPerson(oldData, body);
      logsId.length = 0;
      for (let change of changeArr) {
        updateLogsData(change);
        let result = await readId();
        logsId.push(result);
        if (logsId.length == changeArr.length) {
          let emails = await readEmails();
          let changedData = await readLastLogsData(logsId);
          console.log("changedData" + changedData);
          sendEmails(changedData, emails).catch(console.error);
        }
      }
    }

    if (request.method === "OPTIONS") {
      response.end();
    }
  })
  .listen(8080);

function parseData(data, version) {
  const newData = [];
  data.forEach(element => {
    newData.push({
      id: element.id,
      searchValue: JSON.parse(element[version])[searchValue]
    });
  });
  // console.log('newData ' + newData);
  return newData;
}

function changesInPerson(oldVal, newVal) {
  console.log(oldVal, newVal);
  let firstChunk = [mainUser, variant];
  let result = [];
  let oldData = Object.values(oldVal);
  let newData = Object.values(newVal);
  let keys = Object.keys(oldVal);

  let check = oldData.filter((elem, index) =>
    elem !== newData[index] ? elem : false
  );

  for (let changeValue of check) {
    const changeIndex = oldData.findIndex(elem => elem === changeValue);
    result.push([
      ...firstChunk,
      keys[changeIndex],
      oldData[changeIndex],
      newData[changeIndex]
    ]);
  }

  return result;
}

var connectDB = body =>
  new Promise(async function(resolve, reject) {
    const { user, dbName, password, tableName } = body;
    mainUser = user;
    connection = mysql.createConnection({
      host: "localhost",
      user: user,
      database: dbName,
      password: password
    });
    connection.connect(function(err) {
      if (err) {
        console.error("error connecting: " + err.stack);
        reject(err.stack);
      }
      connection.query(`SELECT * FROM ${tableName}`, function(err, result) {
        if (err) {
          reject(err.stack);
        }
        console.log(result);
        resolve("Connected");
      });
    });
  });

function disconnectDB() {
  connection.end();
}

var getBody = request =>
  new Promise(async function(resolve, reject) {
    let body = [];
    request
      .on("data", chunk => {
        body.push(chunk);
      })
      .on("end", () => {
        body = JSON.parse(body);
        resolve(body);
      });
  });

var readPersonData = (version, id) =>
  new Promise(async function(resolve, reject) {
    sql = `SELECT id, ${version} FROM person WHERE id=${id}`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }

      oldData = JSON.parse(result[0][variant]);
      resolve(oldData);
    });
  });

var readList = (version, id) =>
  new Promise(async function(resolve, reject) {
    sql = `SELECT id, ${version} FROM person`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = parseData(result, version);
      resolve(resultData);
    });
  });

var readLogsData = version =>
  new Promise(async function(resolve, reject) {
    const sql = `SELECT * FROM person_logs WHERE version='${version}'`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = JSON.stringify(result);
      resolve(resultData);
    });
  });

var updatePersonData = (version, body, id) =>
  new Promise(async function(resolve, reject) {
    let sql = `UPDATE person SET ${version}='${body}' WHERE id='${id}'`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = result;
      resolve(resultData);
    });
  });

var updateLogsData = change =>
  new Promise(async function(resolve, reject) {
    const sql = `INSERT INTO person_logs(user, version, val, old_value, new_value) VALUES('${change[0]}','${change[1]}','${change[2]}','${change[3]}','${change[4]}')`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = JSON.stringify(result);
      resolve(resultData);
    });
  });

var readId = () =>
  new Promise(async function(resolve, reject) {
    const sql = `SELECT MAX(id) FROM person_logs`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = result[0]["MAX(id)"];
      resolve(resultData);
    });
  });

var readEmails = () =>
  new Promise(async function(resolve, reject) {
    let receivers = "zhuravleva.caritas@gmail.com";
    const sql = `SELECT email FROM update_emails_persons`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      result.forEach(item => {
        receivers = receivers.concat(`, ${item.email}`);
      });
      resolve(receivers);
    });
  });

var readLastLogsData = logsId =>
  new Promise(async function(resolve, reject) {
    const sql = `SELECT * FROM person_logs WHERE id IN (${logsId.join(",")})`;
    connection.query(sql, function(err, result) {
      if (err) {
        reject(err);
      }
      const resultData = result;
      resolve(resultData);
    });
  });
