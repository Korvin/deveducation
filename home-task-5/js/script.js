let sign;
let firstValue;
let secondValue;
let refreshInput = false;
let result;
const labelDegree = document.getElementById('deg').nextElementSibling;
const labelRadian = document.getElementById('rad').nextElementSibling;

const inputScreen = document.getElementById('input-screen');
const btnArea = document.getElementById('btn-area');


btnArea.addEventListener('click', selectButton);

function selectButton(event) {
    const buttonType = event.target.dataset.buttonType;
    if (!buttonType) {
        return;
    }
    switch(buttonType) {
        case 'clear':
            cleanCalc();
            break;
        case 'delete':
            inputScreen.value = deleteLastNumber(inputScreen.value);
            break;
        case 'equal':
            countNumbers();
            break;
        case 'point':
            addPoint(inputScreen.value);
            break;
        case 'sign':
            addSign(event.target.value);
            break;
        case 'number':
            addNumber(event.target.value);
            break;
        case 'rad':
        case 'deg':
            changeSystem();
            break;
        case 'factorial':
            showFactorial(inputScreen.value);
            break;
        case 'ln':
            countLog(inputScreen.value);
            break;
        case 'log':
            countLogTen(inputScreen.value);
            break;
        case 'radical':
            countRadical(inputScreen.value);
            break;
        case 'degree':
            countDegree(inputScreen.value);
            break;
        case 'exp':
            countExp(inputScreen.value);
            break;
        case 'pi':
            inputScreen.value = Math.PI;
            break;
        case 'e':
            inputScreen.value = Math.E;
            break;
        case 'sin':
            countSinus(inputScreen.value);
            break;
        case 'cos':
            countСosine(inputScreen.value);
            break;
        case 'tan':
            countTanDeg(inputScreen.value);
            break;
    }
}

function cleanCalc() {
    sign = undefined;
    firstValue = undefined;
    secondValue = undefined;
    result = undefined;
    refreshInput = false;
    inputScreen.value = '';
}

function changeSystem() {
    labelRadian.classList.toggle('btn-disabled');
    labelDegree.classList.toggle('btn-disabled');
}

function showFactorial(num) {
    num = +num;
    refreshInput = true;
    inputScreen.value = countFactorial(num);
}

function countFactorial(num) {
    if (!Number.isInteger(num)) {
        return inputScreen.value = '';
    }
    return num ? num * countFactorial(num - 1) : 1;
}

function countLog(num) {
    inputScreen.value = Math.log(num);
}

function countLogTen(num) {
    inputScreen.value = Math.log10(num);
}

function countRadical(num) {
    inputScreen.value = Math.sqrt(num);
}

function countDegree(num) {
    let degree = +prompt(`to what extent do you want to raise 5`, '1');
    inputScreen.value = Math.pow(num, degree);
}

function countExp(num) {
    num = +num;
    let zero = +prompt(`enter in number amount of zeros`, '1');
	
    inputScreen.value = num * Math.pow(10, zero);      
}

function countSinus(num) {
    if (inputScreen.value === '') {
        return Error;
    } else if (labelRadian.classList.value === 'btn-disabled') {
        return inputScreen.value = Math.sin(Math.PI / 180 * num);
    }

    inputScreen.value = Math.sin(num);
}

function countСosine(num) {
    if (inputScreen.value === '') {
        return Error;
    } else if (labelRadian.classList.value === 'btn-disabled') {
        return inputScreen.value = Math.cos(Math.PI / 180 * num);
    }

    inputScreen.value = Math.cos(num);
}

function countTanDeg(num) {
    if (inputScreen.value === '') {
        return Error;
    } else if (labelRadian.classList.value === 'btn-disabled') {
        return inputScreen.value = Math.tan(Math.PI / 180 * num);
    }

    inputScreen.value = Math.tan(num);
}

function deleteLastNumber(str) {
    return str.substring(0, str.length - 1);
}

function addPoint(str) {
    if (!str.includes('.')) {
        return str += '.';
    }
    return str;
}

function addNumber(number) {
    if (refreshInput) {
        inputScreen.value = '';
    }
    refreshInput = false;
    inputScreen.value += number; 
}

function setFirstValue() {
    if (!isNaN(inputScreen.value)) {
        firstValue = inputScreen.value;
    }
}

function setSecondValue() {
    if (firstValue !== undefined) {
        secondValue = inputScreen.value;
    }
}

function addSign(operation) {
    if (sign && !refreshInput) {
        setSecondValue();
        countNumbers();
    } else {
        setFirstValue();
    }
    sign = operation;
    refreshInput = true;
}

function countNumbers() {
    if (secondValue === undefined || !refreshInput) {
        secondValue = inputScreen.value;
    }

    if (result !== undefined) {
        firstValue = result;
    }
    result = countResult(sign, firstValue, secondValue);
    refreshInput = true;
    inputScreen.value = result;
}

function countResult(operation, a, b) {
    let result;
    switch(operation) {
        case '+':
            result = Number(a) + Number(b);
            break;
        case '-':
            result = Number(a) - Number(b);
            break;
        case '*':
            result = Number(a) * Number(b);
            break;
        case '/':
            result = Number(a) / Number(b);
            break;
        default: 
            return undefined;
    }
    return result;
}
