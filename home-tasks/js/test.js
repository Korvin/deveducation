describe('Testing', function() {

    describe('calcNumbers', function() {

        it (`передаем (2, 5), ожидаем перемножение 10`, function() {
            const a = 2;
            const b = 5;
            const actual = calcNumbers(a, b);
            const expected = 10;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3, 5), ожидаем сложение 8`, function() {
            const a = 3;
            const b = 5;
            const actual = calcNumbers(a, b);
            const expected = 8;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('test', 5), ожидаем false`, function() {
            const a = 'test';
            const b = 5;
            const actual = calcNumbers(a, b);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2, '5'), ожидаем false`, function() {
            const a = 2;
            const b = '5';
            const actual = calcNumbers(a, b);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1.5, 3), ожидаем false`, function() {
            const a = 1.5;
            const b = 3;
            const actual = calcNumbers(a, b);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2, undefined), ожидаем false`, function() {
            const a = 2;
            const b = undefined;
            const actual = calcNumbers(a, b);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('findLocationPoint', function() {

        it (`передаем (3, 5) ожидаем 'right top corner'`, function() {
            const x = 3;
            const y = 5;
            const actual = findLocationPoint(x, y);
            const expected = 'right top corner';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-3, -5) ожидаем 'left bottom corner'`, function() {
            const x = -3;
            const y = -5;
            const actual = findLocationPoint(x, y);
            const expected = 'left bottom corner';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-3, 5) ожидаем 'left top corner'`, function() {
            const x = -3;
            const y = 5;
            const actual = findLocationPoint(x, y);
            const expected = 'left top corner';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3, -5) ожидаем 'right bottom corner'`, function() {
            const x = 3;
            const y = -5;
            const actual = findLocationPoint(x, y);
            const expected = 'right bottom corner';

            assert.strictEqual(actual, expected);
        });
        it(`передаем ('3', 5) ожидаем false`, function() {
            const x = '3';
            const y = 5;
            const actual = findLocationPoint(x, y);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0, 5) ожидаем false`, function() {
            const x = 0;
            const y = 5;
            const actual = findLocationPoint(x, y);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3, undefined) ожидаем false`, function() {
            const x = 3;
            const y = undefined;
            const actual = findLocationPoint(x, y);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('calcPositiveNumbersSum', function() {

        it (`передаем (1, 2, 3) ожидаем = 6`, function() {
            const a = 1;
            const b = 2;
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 6;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1, 2, 3) ожидаем = 5`, function() {
            const a = -1;
            const b = 2;
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 5;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, -2, 3) ожидаем = 4`, function() {
            const a = 1;
            const b = -2;
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 4;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, 2, -3) ожидаем = 3`, function() {
            const a = 1;
            const b = 2;
            const c = -3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 3;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1.5, 2.5, 3) ожидаем 7`, function() {
            const a = 1.5;
            const b = 2.5;
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 7;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1, -2, -3) ожидаем 'no positive numbers'`, function() {
            const a = -1;
            const b = -2;
            const c = -3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 'no positive numbers';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0, 0, 0) ожидаем'no positive numbers'`, function() {
            const a = 0;
            const b = 0;
            const c = 0;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = 'no positive numbers';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined, 2, 3) ожидаем false`, function() {
            const a = undefined;
            const b = 2;
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, '2', 3) ожидаем false`, function() {
            const a = 1;
            const b = '2';
            const c = 3;
            const actual = calcPositiveNumbersSum(a, b, c);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('findBiggerNumber', function() {

        it (`передаем (1, 2, 3) ожидаем = 9`, function() {
            const a = 1;
            const b = 2;
            const c = 3;
            const actual = findBiggerNumber(a, b, c);
            const expected = 9;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, 1, 1) ожидаем = 6`, function() {
            const a = 1;
            const b = 1;
            const c = 1;
            const actual = findBiggerNumber(a, b, c);
            const expected = 6;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, 2, 4) ожидаем = 11`, function() {
            const a = 1;
            const b = 2;
            const c = 4;
            const actual = findBiggerNumber(a, b, c);
            const expected = 11;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1, -1, -1) ожидаем = 2`, function() {
            const a = -1;
            const b = -1;
            const c = -1;
            const actual = findBiggerNumber(a, b, c);
            const expected = 2;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1, -2, -3) ожидаем = -3`, function() {
            const a = -1;
            const b = -2;
            const c = -3;
            const actual = findBiggerNumber(a, b, c);
            const expected = -3;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1, -2, -4) ожидаем = -4`, function() {
            const a = -1;
            const b = -2;
            const c = -4;
            const actual = findBiggerNumber(a, b, c);
            const expected = -4;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0, 2, 4) ожидаем = 9`, function() {
            const a = 0;
            const b = 2;
            const c = 4;
            const actual = findBiggerNumber(a, b, c);
            const expected = 9;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, 0, 4) ожидаем = 8`, function() {
            const a = 1;
            const b = 0;
            const c = 4;
            const actual = findBiggerNumber(a, b, c);
            const expected = 8;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, 2, 0) ожидаем = 6`, function() {
            const a = 1;
            const b = 2;
            const c = 0;
            const actual = findBiggerNumber(a, b, c);
            const expected = 6;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined, 2, 3) ожидаем false`, function() {
            const a = undefined;
            const b = 2;
            const c = 0;
            const actual = findBiggerNumber(a, b, c);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1, '2', 3) ожидаем false`, function() {
            const a = 1;
            const b = '2';
            const c = 3;
            const actual = findBiggerNumber(a, b, c);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('checkStudentRating', function() {

        it (`передаем (0) ожидаем = 'F'`, function() {
            const rating = 0;
            const actual = checkStudentRating(rating);
            const expected = 'F';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (39) ожидаем = 'E'`, function() {
            const rating = 39;
            const actual = checkStudentRating(rating);
            const expected = 'E';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (40) ожидаем = 'D'`, function() {
            const rating = 40;
            const actual = checkStudentRating(rating);
            const expected = 'D';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (74) ожидаем = 'C'`, function() {
            const rating = 74;
            const actual = checkStudentRating(rating);
            const expected = 'C';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (75) ожидаем = 'B'`, function() {
            const rating = 75;
            const actual = checkStudentRating(rating);
            const expected = 'B';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (90) ожидаем 'A'`, function() {
            const rating = 90;
            const actual = checkStudentRating(rating);
            const expected = 'A';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (110) ожидаем false`, function() {
            const rating = 110;
            const actual = checkStudentRating(rating);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем false`, function() {
            const rating = undefined;
            const actual = checkStudentRating(rating);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем строку ('2') ожидаем false`, function() {
            const rating = '2';
            const actual = checkStudentRating(rating);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1.5) ожидаем false`, function() {
            const rating = 1.5;
            const actual = checkStudentRating(rating);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('getSumEvenNumbers', function() {

        it (`передаем (50) ожидаем = 'amount of even numbers = 25, sum of even numbers = 650'`, function() {
            const num = 50;
            const actual = getSumEvenNumbers(num);
            const expected = 'amount of even numbers = 25, sum of even numbers = 650';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (49) ожидаем = 'amount of even numbers = 24, sum of even numbers = 600'`, function() {
            const num = 49;
            const actual = getSumEvenNumbers(num);
            const expected = 'amount of even numbers = 24, sum of even numbers = 600';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (100) ожидаем = false`, function() {
            const num = 100;
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-50) ожидаем = false`, function() {
            const num = -50;
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (50.5) ожидаем = false`, function() {
            const num = 50.5;
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('50') ожидаем = false`, function() {
            const num = '50';
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1) ожидаем = false`, function() {
            const num = 1;
            const actual = getSumEvenNumbers(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('isPrime', function() {

        it (`передаем (2) ожидаем = 'positive number'`, function() {
            const num = 2;
            const actual = isPrime(num);
            const expected = 'positive number';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3) ожидаем = 'positive number'`, function() {
            const num = 3;
            const actual = isPrime(num);
            const expected = 'positive number';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (17) ожидаем = 'positive number'`, function() {
            const num = 17;
            const actual = isPrime(num);
            const expected = 'positive number';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (51) ожидаем = 'composite number'`, function() {
            const num = 51;
            const actual = isPrime(num);
            const expected = 'composite number';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (34) ожидаем = 'composite number'`, function() {
            const num = 34;
            const actual = isPrime(num);
            const expected = 'composite number';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (1) ожидаем = false`, function() {
            const num = 1;
            const actual = isPrime(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (17.5) ожидаем = false`, function() {
            const num = 17.5;
            const actual = isPrime(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('17') ожидаем = false`, function() {
            const num = '17';
            const actual = isPrime(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = isPrime(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1) ожидаем = false`, function() {
            const num = -1;
            const actual = isPrime(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('naturalNumberRoot', function() {

        it (`передаем (1) ожидаем = 1`, function() {
            const num = 1;
            const actual = naturalNumberRoot(num);
            const expected = 1;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2) ожидаем = 1`, function() {
            const num = 2;
            const actual = naturalNumberRoot(num);
            const expected = 1;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3) ожидаем = 1`, function() {
            const num = 3;
            const actual = naturalNumberRoot(num);
            const expected = 1;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (4) ожидаем = 2`, function() {
            const num = 4;
            const actual = naturalNumberRoot(num);
            const expected = 2;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = 5`, function() {
            const num = 25;
            const actual = naturalNumberRoot(num);
            const expected = 5;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0) ожидаем = false`, function() {
            const num = 0;
            const actual = naturalNumberRoot(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (17.5) ожидаем = false`, function() {
            const num = 17.5;
            const actual = naturalNumberRoot(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('17') ожидаем = false`, function() {
            const num = '17';
            const actual = naturalNumberRoot(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = naturalNumberRoot(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1) ожидаем = false`, function() {
            const num = -1;
            const actual = naturalNumberRoot(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('countFuctorial', function() {

        it (`передаем (1) ожидаем = 1`, function() {
            const num = 1;
            const actual = countFuctorial(num);
            const expected = 1;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2) ожидаем = 2`, function() {
            const num = 2;
            const actual = countFuctorial(num);
            const expected = 2;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (3) ожидаем = 6`, function() {
            const num = 3;
            const actual = countFuctorial(num);
            const expected = 6;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (7) ожидаем = 5040`, function() {
            const num = 7;
            const actual = countFuctorial(num);
            const expected = 5040;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0) ожидаем = false`, function() {
            const num = 0;
            const actual = countFuctorial(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2.5) ожидаем = false`, function() {
            const num = 2.5;
            const actual = countFuctorial(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const num = '2';
            const actual = countFuctorial(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = countFuctorial(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1) ожидаем = false`, function() {
            const num = -1;
            const actual = countFuctorial(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('calcSumAmongNumber', function() {

        it (`передаем (1) ожидаем = 1`, function() {
            const num = 1;
            const actual = calcSumAmongNumber(num);
            const expected = 1;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (11) ожидаем = 2`, function() {
            const num = 11;
            const actual = calcSumAmongNumber(num);
            const expected = 2;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (111) ожидаем = 3`, function() {
            const num = 111;
            const actual = calcSumAmongNumber(num);
            const expected = 3;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-11) ожидаем = false`, function() {
            const num = -11;
            const actual = calcSumAmongNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0) ожидаем = false`, function() {
            const num = 0;
            const actual = calcSumAmongNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2.5) ожидаем = false`, function() {
            const num = 2.5;
            const actual = calcSumAmongNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const num = '2';
            const actual = calcSumAmongNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = calcSumAmongNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('reverseNumber', function() {

        it (`передаем (123) ожидаем = 321`, function() {
            const num = 123;
            const actual = reverseNumber(num);
            const expected = 321;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (4321) ожидаем = 1234`, function() {
            const num = 4321;
            const actual = reverseNumber(num);
            const expected = 1234;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1) ожидаем = false`, function() {
            const num = -1;
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-11) ожидаем = false`, function() {
            const num = -11;
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0) ожидаем = false`, function() {
            const num = 0;
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2.5) ожидаем = false`, function() {
            const num = 2.5;
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const num = '2';
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = reverseNumber(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('minArrayElement', function() {

        it (`передаем [1, 2, 3] ожидаем = 1`, function() {
            const arr = [1, 2, 3];
            const actual = minArrayElement(arr);
            const expected = 1;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1, -2, 3] ожидаем = -2`, function() {
            const arr = [1, -2, 3];
            const actual = minArrayElement(arr);
            const expected = -2;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = minArrayElement(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = minArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = minArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = minArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = minArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = minArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });    
    });

//-----------------------------------------------------------------------------
    
    describe('maxArrayElement', function() {

        it (`передаем [1, 2, 8] ожидаем = 8`, function() {
            const arr = [1, 2, 8];
            const actual = maxArrayElement(arr);
            const expected = 8;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1, -2, 3] ожидаем = 3`, function() {
            const arr = [1, -2, 3];
            const actual = maxArrayElement(arr);
            const expected = 3;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, -2, -3] ожидаем = -1`, function() {
            const arr = [-1, -2, -3];
            const actual = maxArrayElement(arr);
            const expected = -1;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = maxArrayElement(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = maxArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = maxArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = maxArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });

        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = maxArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = maxArrayElement(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('minArrayIndex', function() {

        it (`передаем [1, 2, 8] ожидаем = 0`, function() {
            const arr = [1, 2, 8];
            const actual = minArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [8, -2, 3] ожидаем = 1`, function() {
            const arr = [8, -2, 3];
            const actual = minArrayIndex(arr);
            const expected = 1;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1, 2, -3] ожидаем = 2`, function() {
            const arr = [1, 2, -3];
            const actual = minArrayIndex(arr);
            const expected = 2;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-11, -2, -8] ожидаем = 0`, function() {
            const arr = [-11, -2, -8];
            const actual = minArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = minArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = minArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = minArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = minArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = minArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = minArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('maxArrayIndex', function() {

        it (`передаем [1, 2, 8] ожидаем = 2`, function() {
            const arr = [1, 2, 8];
            const actual = maxArrayIndex(arr);
            const expected = 2;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [8, -2, 3] ожидаем = 0`, function() {
            const arr = [8, -2, 3];
            const actual = maxArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1, 2, -3] ожидаем = 1`, function() {
            const arr = [1, 2, -3];
            const actual = maxArrayIndex(arr);
            const expected = 1;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, -2, -3] ожидаем = 0`, function() {
            const arr = [-1, -2, -3];
            const actual = maxArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = maxArrayIndex(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = maxArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = maxArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = maxArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = maxArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = maxArrayIndex(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('sumOddArrayNumbers', function() {

        it (`передаем [1, 2, 3] ожидаем = 4`, function() {
            const arr = [1, 2, 3];
            const actual = sumOddArrayNumbers(arr);
            const expected = 4;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [3, 3, 3] ожидаем = 9`, function() {
            const arr = [3, 3, 3];
            const actual = sumOddArrayNumbers(arr);
            const expected = 9;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [3, -3, 3] ожидаем = 3`, function() {
            const arr = [3, -3, 3];
            const actual = sumOddArrayNumbers(arr);
            const expected = 3;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-3, -3, 3] ожидаем = -3`, function() {
            const arr = [-3, -3, 3];
            const actual = sumOddArrayNumbers(arr);
            const expected = -3;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = sumOddArrayNumbers(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = sumOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = sumOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25
            const actual = sumOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = sumOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = sumOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('reverseArray', function() {

        it (`передаем [1, 2, 3] ожидаем = [3, 2, 1]`, function() {
            const arr = [1, 2, 3];
            const actual = reverseArray(arr);
            const expected = [3, 2, 1];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-2, 2, 3] ожидаем = [3, 2, -2]`, function() {
            const arr = [-2, 2, 3];
            const actual = reverseArray(arr);
            const expected = [3, 2, -2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [3, 3, 3] ожидаем = [3, 3, 3]`, function() {
            const arr = [3, 3, 3];
            const actual = reverseArray(arr);
            const expected = [3, 3, 3];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1, 2, 3, 4] ожидаем = [4, 3, 2, 1]`, function() {
            const arr = [1, 2, 3, 4];
            const actual = reverseArray(arr);
            const expected = [4, 3, 2, 1];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [1] ожидаем = [1]`, function() {
            const arr = [1];
            const actual = reverseArray(arr);
            const expected = [1];

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = reverseArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = reverseArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25
            const actual = reverseArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = reverseArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined
            const actual = reverseArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });  
    });

//-----------------------------------------------------------------------------
    
    describe('countOddArrayNumbers', function() {

        it (`передаем [1, 2, 3] ожидаем = 2`, function() {
            const arr = [1, 2, 3];
            const actual = countOddArrayNumbers(arr);
            const expected = 2;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [2, 2, 3] ожидаем = 1`, function() {
            const arr = [2, 2, 3];
            const actual = countOddArrayNumbers(arr);
            const expected = 1;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [3, 3, 3] ожидаем = 3`, function() {
            const arr = [3, 3, 3];
            const actual = countOddArrayNumbers(arr);
            const expected = 3;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [2, 2, 2] ожидаем = 0`, function() {
            const arr = [2, 2, 2];
            const actual = countOddArrayNumbers(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [0, 0, 0] ожидаем = 0`, function() {
            const arr = [0, 0, 0];
            const actual = countOddArrayNumbers(arr);
            const expected = 0;

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = countOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = countOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = countOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = countOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = countOddArrayNumbers(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        }); 
    });

//-----------------------------------------------------------------------------
    
    describe('reverseHalfArray', function() {

        it (`передаем [1, 2, 3, 4] ожидаем = [3, 4, 1, 2]`, function() {
            const arr = [1, 2, 3, 4];
            const actual = reverseHalfArray(arr);
            const expected = [3, 4, 1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2, -3] ожидаем = [2, -3, -1]`, function() {
            const arr = [-1, 2, -3];
            const actual = reverseHalfArray(arr);
            const expected = [2, -3, -1];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2] ожидаем = [2, -1]`, function() {
            const arr = [-1, 2];
            const actual = reverseHalfArray(arr);
            const expected = [2, -1];

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = reverseHalfArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = reverseHalfArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = reverseHalfArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = reverseHalfArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = reverseHalfArray(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('bubbleSort', function() {

        it (`передаем [4, 3, 2, 1] ожидаем = [1, 2, 3, 4]`, function() {
            const arr = [4, 3, 2, 1];
            const actual = bubbleSort(arr);
            const expected = [1, 2, 3, 4];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2, -3] ожидаем = [-3, -1, 2]`, function() {
            const arr = [-1, 2, -3];
            const actual = bubbleSort(arr);
            const expected = [-3, -1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2] ожидаем = [-1, 2]`, function() {
            const arr = [-1, 2];
            const actual = bubbleSort(arr);
            const expected = [-1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = bubbleSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = bubbleSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = bubbleSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = bubbleSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = bubbleSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        
    });

//-----------------------------------------------------------------------------
    
    describe('selectSort', function() {

        it (`передаем [4, 3, 2, 1] ожидаем = [1, 2, 3, 4]`, function() {
            const arr = [4, 3, 2, 1];
            const actual = selectSort(arr);
            const expected = [1, 2, 3, 4];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2, -3] ожидаем = [-3, -1, 2]`, function() {
            const arr = [-1, 2, -3];
            const actual = selectSort(arr);
            const expected = [-3, -1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2] ожидаем = [-1, 2]`, function() {
            const arr = [-1, 2];
            const actual = selectSort(arr);
            const expected = [-1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = selectSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = selectSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = selectSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = selectSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = selectSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('insertionSort', function() {

        it (`передаем [4, 3, 2, 1] ожидаем = [1, 2, 3, 4]`, function() {
            const arr = [4, 3, 2, 1];
            const actual = insertionSort(arr);
            const expected = [1, 2, 3, 4];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2, -3] ожидаем = [-3, -1, 2]`, function() {
            const arr = [-1, 2, -3];
            const actual = insertionSort(arr);
            const expected = [-3, -1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем [-1, 2] ожидаем = [-1, 2]`, function() {
            const arr = [-1, 2];
            const actual = insertionSort(arr);
            const expected = [-1, 2];

            assert.deepEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = insertionSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = insertionSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = insertionSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = insertionSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = insertionSort(arr);
            const expected = false;

            assert.deepEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('getDayName', function() {

        it (`передаем (1) ожидаем = 'Monday'`, function() {
            const num = 1;
            const actual = getDayName(num);
            const expected = 'Monday';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2) ожидаем = 'Tuesday'`, function() {
            const num = 2;
            const actual = getDayName(num);
            const expected = 'Tuesday';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (7) ожидаем = 'Sunday'`, function() {
            const num = 7;
            const actual = getDayName(num);
            const expected = 'Sunday';

            assert.strictEqual(actual, expected);
        });
        it (`передаем (0) ожидаем = false`, function() {
            const num = 0;
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (2.5) ожидаем = false`, function() {
            const num = 2.5;
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const num = '2';
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const num = undefined;
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (-1) ожидаем = false`, function() {
            const num = -1;
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (9) ожидаем = false`, function() {
            const num = 9;
            const actual = getDayName(num);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

//-----------------------------------------------------------------------------
    
    describe('getDistance', function() {

        it (`передаем [[1, 1], [2, 2]] ожидаем = 1.4`, function() {
            const arr = [[1, 1], [2, 2]];
            const actual = getDistance(arr);
            const expected = 1.4;

            assert.strictEqual(actual, expected);
        });
        it (`передаем [[0, 0], [2, 2]] ожидаем = 2.8`, function() {
            const arr = [[0, 0], [2, 2]];
            const actual = getDistance(arr);
            const expected = 2.8;

            assert.strictEqual(actual, expected);
        });
        it (`передаем [[1, 1], [2, 2], [3, 3]] ожидаем = false`, function() {
            const arr = [[1, 1], [2, 2], [3, 3]];
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем [1, 1] ожидаем =  false`, function() {
            const arr = [1, 1];
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем { } ожидаем = false`, function() {
            const arr = {};
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем [ ] ожидаем = false`, function() {
            const arr = [];
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (25) ожидаем = false`, function() {
            const arr = 25;
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем ('2') ожидаем = false`, function() {
            const arr = '2';
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
        it (`передаем (undefined) ожидаем = false`, function() {
            const arr = undefined;
            const actual = getDistance(arr);
            const expected = false;

            assert.strictEqual(actual, expected);
        });
    });

});
