//1     Получить тип входящего значения

function getType(param) {
    return typeof(param);
}

getType('54');


//2     Произвести конкатенацию строк

function getWholeString(one, two, three) {
    return one + two + three;
}

getWholeString('one', 'two', 'three');


//3     Случайным образом получить число от 0 до (не включая) 1 и вывести если > 0.5 'URA' иначе 'NE URA'

function getResult() {
    return Math.random() > 0.5 ? 'URA' : 'NE URA';
}

getResult();


//4     Переписать из входящего массива значения в обьект и вернуть его

function getObject(arr) {
    let newObj = {};

    arr.forEach(function(item, index) {
        newObj[item] = index;
    });

    return newObj;
}

getObject(['test', 'test1']);
