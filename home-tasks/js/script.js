
//1.	Если а – четное посчитать а*б, иначе а+б

function calcNumbers(a, b) {
    switch(false) {
        case (Number.isInteger(a)):
        case (Number.isInteger(b)):
            return false;
    } 
    return (a % 2 === 0) ? (a * b) : (a + b);
} 


//2.	Определить какой четверти принадлежит точка с координатами (х,у)

function findLocationPoint(x, y) {
    switch(true) {
        case (typeof(x) === 'string'):
        case (typeof(y) === 'string'):
        case (x === undefined):
        case (y === undefined):
        case (x === 0):
        case (y === 0):
            return false;
    } 
    return (x > 0 && y > 0) ? 'right top corner' :
        (x < 0 && y < 0) ? 'left bottom corner' :
        (x > 0 && y < 0) ? 'right bottom corner' : 'left top corner';
}


//3.	Найти суммы только положительных из трех чисел

function calcPositiveNumbersSum(a, b, c) {
    switch(true) {
        case (a === undefined):
        case (b === undefined):
        case (c === undefined):
        case (typeof(a) === 'string'):
        case (typeof(b) === 'string'):
        case (typeof(c) === 'string'):
            return false;
    }
    return (a <= 0 && b <= 0 && c <= 0) ? 'no positive numbers' :
        (a > 0 && b > 0 && c > 0) ? a + b + c :
        (a > 0 && b > 0 && c <= 0) ? a + b :
        (a > 0 && b <= 0 && c > 0) ? a + c :
        (a <= 0 && b > 0 && c > 0) ? b + c :
        (a <= 0 && b <= 0 && c > 0) ? c :
        (a <= 0 && b > 0 && c <= 0) ? b : a;
}


//4.	Посчитать выражение макс(а*б*с, а+б+с)+3

function findBiggerNumber(a, b, c) {
    switch(false) {
        case Number.isFinite(a):
        case Number.isFinite(b):
        case Number.isFinite(c):
            return false;
    } 
    return Math.max(a * b * c, a + b + c) + 3;
} 


/*5.	Написать программу определения оценки студента по его рейтингу,
 на основе следующих правил*/

function checkStudentRating(rating) {
    switch(true) {
        case !Number.isInteger(rating):
        case (rating > 100):
            return false;
    }
    return rating >= 0 && rating <= 19 ? 'F' :
        rating > 19 && rating <= 39 ? 'E' :
        rating > 39 && rating <= 59 ? 'D' :
        rating > 59 && rating <= 74 ? 'C' :
        rating > 74 && rating <= 89 ? 'B' : 'A';
}



//1.	Найти сумму четных чисел и их количество в диапазоне от 1 до 99

function getSumEvenNumbers(num) {
    let sum = 0;
    let counter = 0;

    switch(true) {
        case !Number.isInteger(num):
        case (num >= 100):
        case (num <= 1):
            return false;
    }

    for (let i = 1; i <= num; i++) {
        if (i % 2 === 0) {
            sum += i;
            counter++;
        }       
    }
    return `amount of even numbers = ${counter}, sum of even numbers = ${sum}`;   
}

    
//2.	Проверить простое ли число? (число называется простым, если оно делится только само на себя и на 1)

function isPrime(num) {
    switch(true) {
        case !Number.isInteger(num):
        case (num < 2):
            return false;
    }

    for (let i = 2; i < num; i++) {
        if (num % i === 0) {
            return 'composite number';
        }
    }
    return 'positive number';
}


//3.	Найти корень натурального числа с точностью до целого (рассмотреть вариант последовательного подбора и метод бинарного поиска)

// function naturalNumberRoot(num) {
//     switch(true) {
//         case !Number.isInteger(num):
//         case (num <= 0):
//             return false;
//     }
//     return Math.floor(Math.sqrt(num));
// }

function naturalNumberRoot(num) {
    switch(true) {
        case !Number.isInteger(num):
        case (num <= 0):
            return false;
        case (num === 1 || num === 2):
            return 1;
    }

    for (let i = 1; i < num; i++) {
        if ((i * i) > num) {
            return (i - 1);
        }
    }
}

// function naturalNumberRoot(num) {
//     let count = num;
//     let secondCount;

//     switch(true) {
//         case !Number.isInteger(num):
//         case (num <= 0):
//             return false;
//     }

//     while (num * num > count) {
//         num = Math.floor(num / 2);
//     }

//     for (secondCount = num; secondCount <= num * 2; secondCount++) {
//         if (secondCount * secondCount > count) {
//             return secondCount - 1;
//         } else if (secondCount * secondCount === count) {
//             return secondCount;
//         }
//     }
//     return secondCount - 1;
// }


//4.	Вычислить факториал числа n. n! = 1*2*…*n-1*n;!

function countFuctorial(num) {
    switch(true) {
        case !Number.isInteger(num):
        case (num <= 0):
            return false;
        case (num === 1):
            return 1;
    }
    return num * countFuctorial(num - 1);
}


//5.	Посчитать сумму цифр заданного числа

function calcSumAmongNumber(num) {
    let result = '';
    result += num;

    switch(true) {
        case !Number.isInteger(num):
        case (num <= 0):
            return false;
    }

    return result.split('').reduce((sum, current) => sum + +current, 0);
}


//6.	Вывести число, которое является зеркальным отображением последовательности цифр заданного числа, например, задано число 123, вывести 321.

function reverseNumber(num) {
    let result = '';
    result += num;

    switch(true) {
        case !Number.isInteger(num):
        case (num <= 0):
            return false;
    }

    return +(result.split('').reverse().join(''));
}


//1.	Найти минимальный элемент массива

function minArrayElement(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        return arr.sort((a, b) => a - b)[0];
    }
    return false
}


//2.	Найти максимальный элемент массива

function maxArrayElement(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        return arr.sort((a, b) => b - a)[0];
    }
    return false
}


//3.	Найти индекс минимального элемента массива

function minArrayIndex(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        return arr.indexOf(Math.min(...arr));
    }
    return false;
}


//4.	Найти индекс максимального элемента массива

function maxArrayIndex(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        return arr.indexOf(Math.max(...arr));
    }
    return false;
}


//5.	Посчитать сумму элементов массива с нечетными индексами 

function sumOddArrayNumbers(arr) {
    let result = 0;

    if (Array.isArray(arr) && arr.length !== 0) {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] % 2) {
                result += arr[i];
            }
        }
        return result;
    }   
    return false;
}


//6.	Сделать реверс массива (массив в обратном направлении) 

function reverseArray(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        return arr.reverse();
    }
    return false;
}


//7.	Посчитать количество нечетных элементов массива

function countOddArrayNumbers(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        let counter = arr.filter(item => item % 2 !== 0);
        return counter.length;
    }
    return false;
}


//8.	Поменять местами первую и вторую половину массива, например, для массива 1 2 3 4, результат 3 4 1 2

function reverseHalfArray(arr) {
    if (Array.isArray(arr) && arr.length !== 0) {
        let firstHalfArray = arr.slice(0, arr.length / 2);
        let secondHalfArray = arr.slice(arr.length / 2);

        return [...secondHalfArray, ...firstHalfArray];
    }
    return false;
}


//9.	Отсортировать массив (пузырьком (Bubble), выбором (Select), вставками (Insert)) 

function bubbleSort(data) {
    if (Array.isArray(data) && data.length !== 0) {
        for (let i = data.length - 1; i > 0; i--) {
            for (let j = 0; j < i; j++) {
                if (data[j] > data[j + 1]) {
                    let elem = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = elem;
                }
            }
        }
        return data;
    }
    return false;
}


function selectSort(data) {
    if (Array.isArray(data) && data.length !== 0) {
        for (let i = 0; i < data.length; i++) {
            let min = i;
    
            for (let j = i + 1; j < data.length; j++) {
                if (data[j] < data[min]) {
                    min = j;
                }
            }
            let elem = data[i];
            data[i] = data[min];
            data[min] = elem;
        }
        return data;
    }
    return false;
    
}


function insertionSort(data) {
    if (Array.isArray(data) && data.length !== 0) {
        for (let i = 0; i < data.length - 1; i++) {		
            for (let j = i + 1; j < data.length; j++) {
                if (data[i] > data[j]) {
                    let elem = data[j];
                    data[j] = data[i]
                    data[i] = elem;
                }
            }
        }
        return data;
    }
    return false;
    
}


//10.	Отсортировать массив (Quick, Merge, Shell, Heap)

function quickSort(data) {
    if (data.length <= 1) {
        return data;
    }

    let left = [];
    let right = [];
    
    for(let i = 1; i < data.length; i++) {
        if(data[i] < data[0]) {
            left.push(data[i]);
        } else {
            right.push(data[i]);
        }
    }
    
    left = quickSort(left);
    right = quickSort(right);
    left.push(data[0]);
    return left.concat(right);
}


//1.	Получить строковое название дня недели по номеру дня. 

function getDayName(num) {
    let days = ['Monday', 'Tuesday', 'Wensday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

    switch(true) {
        case !Number.isInteger(num):
        case (num <= 0):
        case (num > 7):
            return false;
    }
    return days[num - 1];
}


//2.	Найти расстояние между двумя точками в двухмерном декартовом пространстве

function getDistance(arr) {
    switch(true) {
        case (!(Array.isArray(arr))):
        case (arr.length !== 2):
        case (arr[0][0] === undefined):
        case (arr[0][1] === undefined):
        case (arr[1][0] === undefined):
        case (arr[1][1] === undefined):
            return false;
    }
    return +(Math.sqrt((arr[0][0] - arr[1][0]) * (arr[0][0] - arr[1][0]) + (arr[0][1] - arr[1][1]) * (arr[0][1] - arr[1][1]))).toFixed(1);
}