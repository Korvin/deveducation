class LList {
    root = null;
    tail = null;
    length = 0;

    createNode(value) {
        return {
            value,
            next: null,
            prev: null,
        };
    }

    push(...rest) {

        let lastNode = this.tail;

        // if (lastNode) {
        //     while(lastNode.next) {
        //         lastNode = lastNode.next;
        //     }
        // }

        for (const value of rest) {
            const node = this.createNode(value);

            if (!lastNode) {
                this.root = node;
                this.tail = node;
                lastNode = node;
                continue;
            }

            node.prev = lastNode;
            lastNode.next = node;
            lastNode = lastNode.next;
            this.tail = node;
        }

        this.length += rest.length;

        return this.length;
    }

    pop() {
        let deletedElement = void 0;
        let node = this.tail;

        // if (!node) {
        //     return deletedElement;
        // } else if (!node.next) {
        //     deletedElement = node.value;
        //     this.root = null;
        //     return deletedElement;
        // } else if (!node.next.next) {
        //     deletedElement = node.next.value;
        //     node.next = null;
        //     return deletedElement;
        // }

        if (!node) {
            return deletedElement;
        } else if (!node.prev) {
            deletedElement = node.value;
            node = null;
            return deletedElement;
        }

        deletedElement = node;

        this.tail = this.tail.prev;
        this.tail.next = null;
        this.length = this.length - 1;

        return deletedElement.value;

        // deletedElement = node.next.value;
        // node.next = null;
        // this.length = this.length - 1;

        // return deletedElement;
    }

    unshift(...rest) {
        if (!rest.length) {
            return this.length;
        }

        let newFirstNode = null;
        let firstNode = null;

        for (const value of rest) {
            const node = this.createNode(value);

            if (!firstNode) {
                firstNode = node;
                newFirstNode = node;
            } else {
                node.prev = firstNode;
                firstNode.next = node;
                firstNode = firstNode.next;
            }
        }

        this.tail = firstNode;
        firstNode.next = this.root;
        this.root = newFirstNode;
        this.length += rest.length;

        return this.length;
    }

    shift() {
        let deletedElement = void 0;

        if (!this.root) {
            return deletedElement;
        }

        deletedElement = this.root.value;
        this.root = this.root.next;
        this.root.prev = null;
        this.length = this.length - 1;

        return deletedElement;
    }

    toString() {
        let printNode = this.root;
        let result = '';

        if (!printNode) {
            return result;
        }

        if (printNode) {
            while(printNode.next) {
                result += `${printNode.value},`;
                printNode = printNode.next;
            }
            result += printNode.value;
        }

        return result;
    }
}

const lList = new LList();


console.log(lList.unshift(1,2));
console.log(lList.shift());
console.log(lList.push(55,56,57));
console.log(lList.pop());
console.log(lList.unshift(-1,0));
console.log(lList.shift());
console.log(lList.toString());