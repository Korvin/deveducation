describe('Testing', function() {

    describe('check lList method .push()', function() {

        it (`store = 0, store.length = 0, set 1, expected store.length = 1`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;

            const actual = lList.push(1);

            const expected = 1;
            assert.strictEqual(actual, expected);
        });

        it (`store = 0, store.length = 0, set (1, 2), expected store.length = 2`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;

            const actual = lList.push(1, 2);

            const expected = 2;
            assert.strictEqual(actual, expected);
        });

        it (`store = 0, store.length = 0, set array, expected store.length = 3`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;

            const actual = lList.push(1, 2, 3);

            const expected = 3;
            assert.strictEqual(actual, expected);
        });

        it (`store = [1, 2, 3], store.length = 3, set (3, 4, 5), expected store.length = 6`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(1, 2, 3);

            const actual = lList.push(3, 4, 5);

            const expected = 6;
            assert.strictEqual(actual, expected);
        });

        it (`store = [1, 2], store.length = 2, set (3, 4, 5), expected store.length = 5`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(1, 2);

            const actual = lList.push(3, 4, 5);

            const expected = 5;
            assert.strictEqual(actual, expected);
        });
    });

    describe('check lList method .pop()', function() {

        it (`store = 0, expected = undefine`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;

            const actual = lList.pop();

            const expected = undefined;
            assert.strictEqual(actual, expected);
        });

        it (`store = 5, expected = 5`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(5);

            const actual = lList.pop();

            const expected = 5;
            assert.strictEqual(actual, expected);
        });

        it (`store = (5, 6, 7), expected last = 7`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(5, 6, 7);

            const actual = lList.pop();

            const expected = 7;
            assert.strictEqual(actual, expected);
        });

        it (`store = (3, 4), expected last = 4`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(3, 4);

            const actual = lList.pop();

            const expected = 4;
            assert.strictEqual(actual, expected);
        });

        it (`store = (1, 2, 3, 4, [5, 4]), expected last = [5, 4]`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(1, 2, 3, 4, [5, 4]);

            const actual = lList.pop();

            const expected = [5, 4];
            assert.deepEqual(actual, expected);
        });
    });

    describe('check lList method .shift()', function() {

        it (`store = 0, expected = undefine`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;

            const actual = lList.pop();

            const expected = undefined;
            assert.strictEqual(actual, expected);
        });

        it (`store = 5, expected = 5`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(5);

            const actual = lList.pop();

            const expected = 5;
            assert.strictEqual(actual, expected);
        });

        it (`store = (5, 6, 7), expected = 7`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(5, 6, 7);

            const actual = lList.pop();

            const expected = 7;
            assert.strictEqual(actual, expected);
        });

        it (`store = (3, 4), expected = 4`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(3, 4);

            const actual = lList.pop();

            const expected = 4;
            assert.strictEqual(actual, expected);
        });

        it (`store = (1, 2, 3, 4, [5, 4]), expected = [5, 4]`, function() {
            lList.root = null;
            lList.tail = null;
            lList.length = 0;
            lList.push(1, 2, 3, 4, [5, 4]);

            const actual = lList.pop();

            const expected = [5, 4];
            assert.deepEqual(actual, expected);
        });
    });
});

